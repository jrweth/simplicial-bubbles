//
// Created by Wetherbe Admin on 10/19/18.
//

#ifndef FEM_SOLVER_FEMSOLVER_H
#define FEM_SOLVER_FEMSOLVER_H
#import "geometry/Geometry.h"
#import "eigen/Eigen/Dense"
#import "eigen/Eigen/Jacobi"
#include <iostream>
#include <vector>
#include "force/Force.h"
#include "force/Gravity.h"
#include "utils/GeometryUtils.h"
#include "material/Material.h"
#include <unordered_map>
#include <unordered_set>
#include "config/FemSolverConfig.h"

using  std::cout;
using  std::endl;
using namespace Eigen;

struct BoundaryMeshPart {
    std::unordered_set<int> boundary_triangles = {};
    std::unordered_set<int> boundary_points = {};
};

template<int dim>
class FemSolver {
private:

public:
    FemSolver(FemSolverConfig config);

    FemSolverConfig config;

    //original position
    vector<Matrix<double, dim, 1>> original_position;

    //the "current" positions of each point
    vector<Matrix<double, dim, 1>> position;

    //the velocities of each point
    vector<Matrix<double, dim, 1>> velocity;

    //the cumulative elastic force for each point at this time step
    vector<Matrix<double, dim, 1>> elastic_force;

    //the cumulative elastic force for each point at this time step
    vector<Matrix<double, dim, dim>> prev_F;

    //the collection of mesh elements (triangles or tetrahedra) that make up the mesh
    vector<array<int, dim+1>>  mesh;

    //the volume each mesh element (triangle area or tetrahedra volume)
    vector<double> mesh_volume;

    //the original meshDm (shape matrix)
    vector<Matrix<double, dim, dim>> mesh_Dm;

    //the original meshDm inverse
    vector<Matrix<double, dim, dim>> mesh_DmInv;

    //the mass at each point
    vector<double> point_mass;

    //renumbered list of points in the boundary referencing the point index
    vector<int> boundary_points = {};

    //list of boundary triangles referenencing the boundary point indexes
    vector<array<int, 3>> boundary_triangles;

    //list of tetrahedra which have at least one point on the boundary
    std::unordered_map<int, BoundaryMeshPart> boundary_mesh_parts;

    char outpolyfilename[FILENAMESIZE];
    char outpolyfilepath[FILENAMESIZE];

    //the current frame
    int current_frame = 1;

    double current_time = 0.0;

    //the total time for the simulation
    int total_time = 10;


    int time_steps_per_frame;

    void advanceTimeStep(double time_step);
    void saveFrameFile();
    void setFileName(char* fileName);
    void setFilePath(char* filePath);

protected:
    void init();
    void initPosition();
    void initVelocity();
    void initMesh();
    void initMeshDm();
    void initMeshDmInverse();
    void initMeshVolume();
    void initPointMass();
    void initBoundary();
    void initBoundaryMeshParts();

    void advanceFrame();

    Geometry * geometry;
    void getFrameFileName(char* fileName);
    Matrix<double, dim, dim> getShapeMatrix(array<Matrix<double, dim, 1>, dim + 1>);
    Matrix<double, dim, dim> getMeshShapeMatrix(int meshIndex);

    void computeElasticForces();
    void applyExternalForces();
    Matrix<double, dim, dim> deformationGradient(int meshIndex);
    Matrix<double, dim, dim> neohoockianStress(Matrix<double, dim, dim> F);
    Matrix<double, dim, dim> coRotatedStress(Matrix<double, dim, dim> F);

};

template<int dim>
FemSolver<dim>::FemSolver(FemSolverConfig config): config(config) {
    this->geometry = config.geometry;
    init();
}

template<int dim>
void FemSolver<dim>::setFileName(char *fileName) {
    std::sprintf(outpolyfilename, "%s", fileName);
}

template<int dim>
void FemSolver<dim>::setFilePath(char *filePath) {
    std::sprintf(outpolyfilename, "%s", filePath);
}

template<int dim>
void FemSolver<dim>::init() {

    std::sprintf(outpolyfilename, "%s", "tetra");
    std::sprintf(outpolyfilepath, "%s", "../fem_output/tetra2/");
    current_time = 0.0;

    //get the number of timesteps per frame
    time_steps_per_frame = (double)(1.0 / config.fps) / config.time_step;
    if(time_steps_per_frame ==0 ) time_steps_per_frame = 1;

    cout << "timesteps per frame " << time_steps_per_frame;
    cout << endl << "mu" << config.material.mu;
    cout << endl << "lamda: " << config.material.lamda;
    initPosition();
    initVelocity();
    initMesh();
    initMeshDm();
    initMeshDmInverse();
    initMeshVolume();
    initPointMass();
    initBoundary();
    initBoundaryMeshParts();
}


/**
 * Initilaize the position of all points in our object
 * @tparam dim number of dimensions
 */
template<int dim>
void FemSolver<dim>::initPosition() {

    cout << "initializing position .... ";
    //initilialize points
    int i, j;
    position.clear();
    original_position.clear();
    for (i = 0; i < geometry->getNumPoints(); i++) {
        Matrix<double, dim, 1> p;
        vector<double> points = geometry->getPointPosition(i);
        for(j=0; j < dim; j++) {
            p(j,0) = points[j];
        }
        position.push_back(p);
        original_position.push_back(p);
    }

}


/**
 * initilialize the velocities of all of our points to 0
 * @tparam dim
 */
template<int dim>
void FemSolver<dim>::initVelocity() {

    cout << "initializing velocity .... ";
    //initilialize velocity
    int i, j;
    velocity.clear();
    for (i = 0; i < geometry->getNumPoints(); i++) {
        Matrix<double, dim, 1> v;
        for (j = 0; j < dim; j++) {
            v[j] = 0.0;
        }
        velocity.push_back(v);
    }
}

/**
 * Initiliaze the mesh (either tetrahedra or triangles) and mesh volume
 * @tparam dim
 */
template<int dim>
void FemSolver<dim>::initMesh() {

    cout << "initializing mesh .... ";
    int i,j;
    mesh.clear();
    prev_F.clear();
    //initialize the mesh and mesh volume and prev_F
    for(i = 0; i < geometry->getNumMeshParts(); i++) {
        array<int, dim+1> a;
        array<Matrix<double, dim, 1>, dim+1> positions;
        vector<int> meshParts = geometry->getMeshElementPoints(i);
        for(j=0; j< dim + 1; j++) {
            a[j] = meshParts[j];
            positions[j] = position[meshParts[j]];
        }
        //check to make sure we don't have tetrahedra that are too small
        if(true) {//&& abs(getShapeMatrix(positions).determinant()) / 6.0 > 0.00001) {
            mesh.push_back(a);
            prev_F.push_back(Matrix<double, dim, dim>::Zero());
        }
        else {
            cout << "s";
        }
    }
    cout <<endl << "total mesh parts" << mesh.size();


}

template<int dim>
Matrix<double, dim, dim> FemSolver<dim>::getShapeMatrix(array<Matrix<double, dim, 1>, dim + 1> positions) {
    int i, j;
    Matrix<double, dim, dim> m;
    for(i =0; i < dim; i++) {
        for(j = 0; j < dim; j++) {
            m(j, i) = positions[i](j,0) - positions[dim](j,0);
        }
    }
    return m;
}

template<int dim>
Matrix<double, dim, dim> FemSolver<dim>::getMeshShapeMatrix(int meshIndex) {
    array<Matrix<double, dim, 1>, dim+1> positions;
    for(int i = 0; i < dim +1; i++) {
        positions[i] = position[mesh[meshIndex][i]];
    }

    return getShapeMatrix(positions);
}

template<int dim>
void FemSolver<dim>::initMeshDm() {

    cout << "initializing meshDm .... ";
    mesh_Dm.clear();
    int meshIndex, i;
    for(meshIndex = 0; meshIndex < mesh.size(); meshIndex++) {
        mesh_Dm.push_back(getMeshShapeMatrix(meshIndex));
    }
}

template<int dim>
void FemSolver<dim>::initMeshDmInverse() {
    cout << endl << "initializing meshDmInverse .... ";
    int i;
    mesh_DmInv.clear();
    for(i = 0; i < mesh.size(); i++) {
        Matrix<double, dim, dim> DmInv;
        DmInv = mesh_Dm[i].inverse();
        mesh_DmInv.push_back(DmInv);
    }
}


template<int dim>
void FemSolver<dim>::initMeshVolume() {
    cout << endl << "initializing mesh volume .... ";
    int i;
    mesh_volume.clear();
    for(i = 0; i < mesh.size(); i++) {
        if(dim == 3) {
            mesh_volume.push_back(abs(mesh_Dm[i].determinant() / 6.0));
        }
        else if (dim==2) {
            mesh_volume.push_back(abs(mesh_Dm[i].determinant() / 2));
        }
        if(mesh_volume[i] < 0.00001) cout << "s";
    }
}
/**
 * Initialize the mass of each point by adding 1/4 of the volume of each tetrahedra it belongs to times the material density
 * @tparam dim
 */
template<int dim>
void FemSolver<dim>::initPointMass() {
    cout << endl << "initializing point mass .... ";
    point_mass.clear();
    for(int i=0; i<position.size(); i++) {
       point_mass.push_back(0.0);
    }
    for(int t = 0; t < mesh.size(); t++) {
        for(int pIndex = 0; pIndex < dim + 1; pIndex++) {
            point_mass[mesh[t][pIndex]] += config.material.density * mesh_volume[t] / 4;
        }
    }
    double totalMass = 0.0;
    for(int j = 0; j <point_mass.size() ; j++) {
        totalMass += point_mass[j];
    }
    cout << endl << "totalMass: " << totalMass;

    totalMass = 0.0;
    for(int j = 0; j < mesh_volume.size() ; j++) {
        totalMass += mesh_volume[j];
    }

}


template<int dim>
void FemSolver<dim>::initBoundary() {
    cout << endl << "initializing boundary .... ";
    int i, j;
    std::unordered_map<int, int> pointToFacePoint;
    boundary_points.clear();
    boundary_triangles.clear();
    pointToFacePoint.clear();

    for (i = 0; i < geometry->getNumBoundaryTriangles(); i++) {
        array<int, 3> origTrianglePoints = geometry->getBoundaryTriangle(i);
        array<int, 3> boundaryTriangle({{0,0,0}});
        for(j = 0; j < 3; j++) {
            int pointNum = origTrianglePoints[j];
            if(std::find(boundary_points.begin(), boundary_points.end(), pointNum) == boundary_points.end()) {
                pointToFacePoint.insert({{pointNum, boundary_points.size()}});
                boundary_points.push_back(pointNum);
            }
            boundaryTriangle[j] = pointToFacePoint[pointNum];
        }
        boundary_triangles.push_back(boundaryTriangle);
    }
}

template<int dim>
void FemSolver<dim>::initBoundaryMeshParts() {
    cout << endl << "initializing boundary mesh parts.... ";
    boundary_mesh_parts.clear();
    for(int bT = 0; bT < boundary_triangles.size(); bT ++) {
        //search through each mesh part to see if we have a matching point
        for(int m = 0; m < mesh.size(); m++) {
            int numMatches = 0;
            for(int bTPoint = 0; bTPoint < 3; bTPoint ++) {
                for(int mPoint = 0; mPoint < dim + 1; mPoint++) {
                    //search if we have a match
                    if(mesh[m][mPoint] == boundary_points[boundary_triangles[bT][bTPoint]]) {
                        numMatches++;
                        //see if we have added this mesh part yet
                        if(boundary_mesh_parts.find(m) == boundary_mesh_parts.end()) {
                            BoundaryMeshPart bmp;
                            boundary_mesh_parts.insert({m, bmp});
                        }
                        boundary_mesh_parts[m].boundary_points.insert(mesh[m][mPoint]);
                    }
                }
            }
            if(numMatches == 3) boundary_mesh_parts[m].boundary_triangles.insert(bT);
        }
    }

}
template <int dim>
void FemSolver<dim>::saveFrameFile() {

    FILE *fout;
    char outpolyfilename[FILENAMESIZE];
    int i, j, pointNum;

    getFrameFileName(outpolyfilename);

    fout = fopen(outpolyfilename, "w");


    array<double, 3> adjusted_position;
    fprintf(fout, "POINTS\n" );
    for (i=0; i < boundary_points.size(); i++) {
        pointNum = boundary_points[i];
        for(j = 0; j < dim; j++) {
            adjusted_position[j] = position[pointNum][j];
            if(abs(position[pointNum][j]) < 0.000001) {
                adjusted_position[j] = 0;
            }
        }


        if (dim == 2) {
            fprintf(fout, "%d:  %.16g  %.16g %.16g", i + 1,
                    adjusted_position[0], adjusted_position[1], 0.0);
        } else {
            fprintf(fout, "%d:  %.16g  %.16g  %.16g", i + 1,
                    adjusted_position[0], adjusted_position[1], adjusted_position[2]);
        }
        fprintf(fout, "\n");
    }

    //print out the polys
    fprintf(fout, "POLYS\n");
    for (i = 0; i < boundary_triangles.size(); i++) {
        fprintf(fout, "%d:  %5d  %5d  %5d <\n",
                i + 1,
                boundary_triangles[i][0] + 1,
                boundary_triangles[i][1] + 1,
                boundary_triangles[i][2] + 1
        );
    }
    fprintf(fout, "END\n");
    fclose(fout);

}

template<int dim>
void FemSolver<dim>::getFrameFileName(char* fileName) {
    int thousand = (int)floor(current_frame / 1000);
    int hundred = (int)floor(current_frame / 100) % 10;
    int ten = (int)floor(current_frame /10) % 10;
    int one = current_frame % 10;
    std::sprintf(fileName, "%s%s%s%d%d%d%d%s", outpolyfilepath, outpolyfilename, "." , thousand, hundred, ten, one, ".poly");
}


/**
 * Get the deformation gradient
 * @tparam dim
 * @param meshIndex the index of the tetrahedra
 * @return
 */
template <int dim>
Matrix<double, dim, dim> FemSolver<dim>::deformationGradient(int meshIndex) {
    Matrix<double, dim, dim> m;
    m = getMeshShapeMatrix(meshIndex)*mesh_DmInv[meshIndex];
    for(int i = 0; i < dim; i++) {
        for(int j =0; j < dim; j ++) {
            if(abs(m(i,j)) < 0.000001) m(i,j) = 0.0;
        }
    }
    return m;
}


template<int dim>
void FemSolver<dim>::advanceFrame() {
    int i;
    current_frame++;

    cout << endl << " " << current_frame;

    for(int i = 0; i < time_steps_per_frame; i++) {
        advanceTimeStep();
    }


}
template <int dim>
void FemSolver<dim>::advanceTimeStep(double time_step) {

    int forceIndex, pointIndex;
    Force<dim>* force;
    Matrix<double, dim, 1> pointForce;
    //loop through each mesh element (tetrahedra triangle) and calculate elastic forces

    computeElasticForces();
    Matrix<double, dim, 1> acceleration;
    for(pointIndex = 0; pointIndex < velocity.size(); pointIndex++) {
        pointForce = elastic_force[pointIndex];
        for(forceIndex = 0; forceIndex < config.external_forces.size(); forceIndex++) {
            force = config.external_forces[forceIndex];
            pointForce += force->calculate(this, pointIndex, pointForce);
        }
        if(point_mass[pointIndex] == 0) {
            acceleration = Matrix<double, dim, 1>::Zero();
        }
        else {
            acceleration = pointForce/point_mass[pointIndex];
        }
        velocity[pointIndex] += acceleration * time_step;
        position[pointIndex] += velocity[pointIndex] * time_step;
    }


    //handleCollisions();
    current_time += time_step;
}

template<int dim>
void FemSolver<dim>::computeElasticForces() {
    //set all elastic force to 0
    elastic_force.clear();
    for(int i = 0; i < position.size(); i++) {
        Matrix<double, dim, 1> m;
        m = Matrix<double, dim, 1>::Zero();
        elastic_force.push_back(m);
    }

    bool debug = current_frame <= 0;
    for(int meshIndex = 0; meshIndex < mesh.size(); meshIndex++) {
        Matrix<double, dim, dim> F = deformationGradient(meshIndex);
        Matrix<double, dim, dim> P;
        //hack to handle when a tetrahedron collapses - use co rotated
        P = neohoockianStress(F);
        Matrix<double, dim, dim> H = - mesh_volume[meshIndex] * P * mesh_DmInv[meshIndex].transpose();
        if(debug) {
            cout << endl << endl << "-------------------------" << endl <<"Frame " << current_frame << endl;
            cout << endl << "mesh volume: " << mesh_volume[meshIndex];
            cout << endl << "Points";
            for(int j=0; j <= dim; j++) {
                cout << endl << j << ".  " << position[mesh[meshIndex][j]].transpose();
            }
            cout << endl << "F:" << endl <<F ;
            cout << endl << "P:" << endl <<P ;
            cout << endl << "H:" << endl << H;
        }
        int lastPointIndex = mesh[meshIndex][dim];

        //calculate the first three forces based upon columns of our H matrix
        //calculate the forces on the last as the subtraction of all the others
        if(debug) cout << endl << "elastic forces";
        for(int i = 0; i < dim; i++) {
            int pointIndex = mesh[meshIndex][i];
            for(int j=0; j<dim; j++) {
                double force = H(j,i);
                //clamp the elastic force
                if(force > mesh_volume[meshIndex] * 100000) {
                    force = mesh_volume[meshIndex] * 100000;
                    cout <<"c";
                }
                if(force < -mesh_volume[meshIndex] * 100000) {
                    force = -mesh_volume[meshIndex] * 100000;
                    cout <<"c";
                }
                elastic_force[pointIndex](j,0) += force;
                elastic_force[lastPointIndex](j,0) -= force;
            }
            if(debug) cout << endl << elastic_force[pointIndex].transpose();
        }
        if(debug) cout << endl << elastic_force[lastPointIndex].transpose();
        //calculate the last force based upon the summation of all three columns
    }
}

template <int dim>
Matrix<double, dim, dim> FemSolver<dim>::neohoockianStress(Matrix<double, dim, dim> F){

    Matrix<double, dim, dim> Finv = F.inverse();
    Matrix<double, dim, dim> FinvT = Finv.transpose();
    if(F.determinant() > 0)
    {
        //return mu*F - (mu * FinvT) + (lamda * log(I3) / 2)*FinvT;
        return config.material.mu * (F - FinvT) + config.material.lamda * log(F.determinant())*FinvT;
    }
    else {
        return coRotatedStress(F);
    }
}

template<int dim>
Matrix<double, dim, dim> FemSolver<dim>::coRotatedStress(Matrix<double, dim, dim> F) {

    cout << "r";
    JacobiSVD<Matrix<double, dim, dim>> svd(F, ComputeFullV | ComputeFullU);
    Matrix<double, dim, dim> U = svd.matrixU();
    Matrix<double, dim, dim> V = svd.matrixV();
    if (U.determinant() < 0.0) {
        U.col(2) *= -1;
    }
    if (V.determinant() < 0.0) {
        V.col(2) *= -1;
    }
    Matrix<double, dim, dim> I = Matrix<double, dim, dim>::Identity();

    Matrix<double, dim, dim> R = U * V.transpose();
    if(current_frame ==2)  {
        Matrix<double, dim, dim> f_r = F - R;
        Matrix<double, dim, dim> rt_f_i = I;
    }

    return 2.0f * config.material.mu * (F - R) + config.material.lamda * (((R.transpose()*F) - I).trace())*R;

}
#endif //FEM_SOLVER_FEMSOLVER_H
