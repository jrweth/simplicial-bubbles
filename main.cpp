#include <iostream>
#include <Partio.h>
#include "geometry/Cube.h"
#include "geometry/SingleTetrahedron.h"
#include "FemSolver.h"
#include "material/Jello.h"
#include "force/Push.h"
#include "force/Damper.h"
#include "force/InitStretch.h"
#include "geometry/Triangle.h"
#include "geometry/DoubleTetrahedron.h"
#include "config/FemSolverConfig.h"
#include "config/StretchCubeConfig.h"
#include "config/GravityCubeConfig.h"
#include "config/GravityCubeStaircaseConfig.h"
#include "config/GravityCubeStaircasePushConfig.h"
#include "collision/CollisionHandler.h"
#include "collision/GroundPlane.h"
#include "collision/Staircase.h"
#include "collision/Platform.h"
#include "collision/Sphere.h"
#include "collision/OtherFemObjects.h"
#include "config/SingleTetrahedronConfig.h"
#include "config/GravityGummiBearConfig.h"
#include "config/RegularCubeGravityConfig.h"


int main(int argc, char *argv[]) {

//    const int dim = 2;
//    auto *geometry = new Triangle();



    //RegularCubeGravityConfig config = RegularCubeGravityConfig();
    //GravityCubeConfig config = GravityCubeConfig();
    //GravityCubeStaircaseConfig config = GravityCubeStaircaseConfig();
    //GravityCubeStaircasePushConfig config = GravityCubeStaircasePushConfig();
    //GravityGummiBearConfig config = GravityGummiBearConfig();
    //RegularCubeGravityConfig config = RegularCubeGravityConfig();


//    Matrix<double, 3, 1> p1; p1 << 2.5, 0, 0;
//    Matrix<double, 3, 1> p2; p2 << 2.25, 0.0, 0;
//
//    cout << endl << "dot: " << p1.dot(p2) / p1.norm();
//
//    return 0;


    const int dim = 3;


    RegularCubeGravityConfig config = RegularCubeGravityConfig();
    double cos30 = cos(30.0  * 3.14159/180.0);
    double sin30 = sin(30.0 * 3.14159/180.0);
    Matrix<double, 4, 4>  rotate;
    rotate << 1, 0, 0, 0,
            0, cos30   , sin30, 0,
            0, -sin30, cos30, 0,
            0, 0, 0, 1;
    config.geometry->adjustPosition(rotate);
    vector<FemSolver<dim>*> femSolvers = {};
    for(int cube = 0; cube < 3; cube++) {
        Matrix<double, 4, 4> translate;
        translate << 1, 0,0, .7,    0,1,0,1.55,   0,0,1,.65,  0,0,0,1;
        config.geometry->adjustPosition(translate);
        config.initGeometry();
        cout << endl << config.transform;
        femSolvers.push_back(new FemSolver<dim>(config));
        char filenameA[] = {'o', 'b', 'j', '_', std::to_string(cube)[0], '\0'};
        femSolvers[cube]->setFileName(filenameA);


    }
    std::vector<CollisionHandler*> collisions;
    collisions.clear();
    collisions.push_back(new GroundPlane());
    collisions.push_back(new OtherFemObjects());
    //collisions.push_back(new Staircase());
    //collisions.push_back(new Platform());
    //collisions.push_back(new Sphere());

    for(int i = 0; i < collisions.size(); i++) {
        for(int f = 0; f < femSolvers.size(); f++) {
            collisions[i]->addFemObject(femSolvers[f]);
        }
    }


    //get the number of timesteps per frame
    int total_time = 15;
    int fps = 60;
    int time_steps_per_frame = 5;
    double time_step = 1.0 / (fps * time_steps_per_frame);

    int current_frame = 1;
    while(current_frame <= total_time * config.fps) {
        current_frame++;
        //if(current_frame == 89) return 0;
        cout << current_frame << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ " << std::flush;
        //advance frame and steps
        for(int f = 0; f < femSolvers.size(); f++) {
            femSolvers[f]->saveFrameFile();
            femSolvers[f]->current_frame = current_frame;
            for(int i = 0; i < time_steps_per_frame; i++) {
                femSolvers[f]->advanceTimeStep(time_step);
            }
            for(int i = 0; i < collisions.size(); i++) {
                collisions[i]->adjustFemPositions();
            }
        }
    }



    return 0;
}
