//
// Created by Wetherbe Admin on 11/13/18.
//

#ifndef FEM_SOLVER_GRAVITYCUBESTAIRCASEPUSHCONFIG_H
#define FEM_SOLVER_GRAVITYCUBESTAIRCASEPUSHCONFIG_H

#include "FemSolverConfig.h"
#include "../geometry/Cube.h"
#include "../force/InitStretch.h"
#include "../force/Gravity.h"
#include "../force/Damper.h"
#include "../force/Push.h"

struct GravityCubeStaircasePushConfig : public FemSolverConfig {
    GravityCubeStaircasePushConfig():
            FemSolverConfig(
                    60, //fps
                    1.0/240.0, //time_step
                    80, //youngs modulus
                    0.4, //poisson ratio
                    1.0, //density
                    0.01 //maxMeshPartVolume
            )
    {
        geometry = new Cube();
        transform << 1, 0, 0, 1,
                0, 1, 0, 6,
                0, 0, 1, 0,
                0, 0, 0, 1;
        external_forces.clear();

        external_forces.push_back(new InitStretch<3>(0));
        external_forces.push_back(new Gravity<3>(0.2));
        external_forces.push_back(new Damper<3>(.65));
        //external_forces.push_back(new Push<3>(-1,0,0));

    }

};
#endif //FEM_SOLVER_GRAVITYCUBESTAIRCASEPUSHCONFIG_H
