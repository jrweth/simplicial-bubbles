//
// Created by Wetherbe Admin on 11/16/18.
//

#ifndef FEM_SOLVER_REGULARCUBEGRAVITYCONFIG_H
#define FEM_SOLVER_REGULARCUBEGRAVITYCONFIG_H



#include "FemSolverConfig.h"
#include "../geometry/RegularCube.h"
#include "../force/InitStretch.h"
#include "../force/Gravity.h"
#include "../force/Damper.h"


class RegularCubeGravityConfig: public FemSolverConfig {
public:
    RegularCubeGravityConfig():
            FemSolverConfig(
                    60, //fps
                    1.0/480, //time_step
                    25, //youngs modulus
                    0.40, //poisson ratio
                    1.0, //density
                    0.01 //maxMeshPartVolume
            )
    {
        geometry = new RegularCube(2);
        transform << 1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1;
        external_forces.clear();

        external_forces.push_back(new InitStretch<3>(0));
        external_forces.push_back(new Gravity<3>(0.2));
        external_forces.push_back(new Damper<3>(.65));
    }

};
#endif //FEM_SOLVER_REGULARCUBEGRAVITYCONFIG_H
