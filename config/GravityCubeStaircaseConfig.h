//
// Created by Wetherbe Admin on 11/8/18.
//

#ifndef FEM_SOLVER_GRAVITYCUBESTAIRCASECONFIG_H
#define FEM_SOLVER_GRAVITYCUBESTAIRCASECONFIG_H

#include "FemSolverConfig.h"
#include "../geometry/Cube.h"
#include "../force/InitStretch.h"
#include "../force/Gravity.h"
#include "../force/Damper.h"

struct GravityCubeStaircaseConfig : public FemSolverConfig {
    GravityCubeStaircaseConfig():
            FemSolverConfig(
                    60, //fps
                    1.0/240.0, //time_step
                    100, //youngs modulus
                    0.4, //poisson ratio
                    1.0, //density
                    0.01 //maxMeshPartVolume
            )
    {
        geometry = new Cube();
        double cos30 = cos(30.0 * 3.14159/180.0);
        double sin30 = sin(30.0 * 3.14159/180.0);
        transform << 1, 0, 0, -0.3,
                0, 1, 0, 6.5,
                0, 0, 1, 0,
                0, 0, 0, 1;
        external_forces.clear();

        external_forces.push_back(new InitStretch<3>(0));
        external_forces.push_back(new Gravity<3>(0.2));
        external_forces.push_back(new Damper<3>(.65));

    }

};
#endif //FEM_SOLVER_GRAVITYCUBESTAIRCASECONFIG_H
