//
// Created by Wetherbe Admin on 11/15/18.
//

#ifndef FEM_SOLVER_SINGLETETRAHEDRONCONFIG_H
#define FEM_SOLVER_SINGLETETRAHEDRONCONFIG_H

#include "FemSolverConfig.h"
#include "../geometry/SingleTetrahedron.h"
#include "../force/InitStretch.h"
#include "../force/Gravity.h"
#include "../force/Damper.h"

struct SingleTetrahedronConfig: public FemSolverConfig {
public:
    SingleTetrahedronConfig():
            FemSolverConfig(
                    60, //fps
                    1.0/60, //time_step
                    40, //youngs modulus
                    0.4, //poisson ratio
                    1.0, //density
                    0.01 //maxMeshPartVolume
            )
    {
        geometry = new SingleTetrahedron();
        double cos30 = cos(30.0 * 3.14159/180.0);
        double sin30 = sin(30.0 * 3.14159/180.0);
        transform << 1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1;
        external_forces.clear();
        external_forces.push_back(new Gravity<3>(0.2));
        external_forces.push_back(new Damper<3>(.65));

    }
};
#endif //FEM_SOLVER_SINGLETETRAHEDRON_H
