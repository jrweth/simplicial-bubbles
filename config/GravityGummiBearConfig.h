//
// Created by Wetherbe Admin on 11/15/18.
//

#ifndef FEM_SOLVER_GRAVITYGUMMIBEARCONFIG_H
#define FEM_SOLVER_GRAVITYGUMMIBEARCONFIG_H

#include "FemSolverConfig.h"
#include "../geometry/GummiBear.h"
#include "../force/InitStretch.h"
#include "../force/Gravity.h"
#include "../force/Damper.h"

struct GravityGummiBearConfig : public FemSolverConfig {
    GravityGummiBearConfig():
            FemSolverConfig(
                    60, //fps
                    1.0/1800.0, //time_step
                    80, //youngs modulus
                    0.4, //poisson ratio
                    1.0, //density
                    0.03 //maxMeshPartVolume
            )
    {
        geometry = new GummiBear();
        double cos30 = cos(30.0 * 3.14159/180.0);
        double sin30 = sin(30.0 * 3.14159/180.0);
//        transform << 1, 0, 0, 0,
//                0, cos30, sin30, 2,
//                0, -sin30, cos30, 0,
//                0, 0, 0, 1;
        transform << 1, 0, 0, 0,
                0, 1, 0, 4.8,
                0, 0, 1, 0,
                0, 0, 0, 1;
        external_forces.clear();
        external_forces.push_back(new Gravity<3>(.3));
        external_forces.push_back(new Damper<3>(.8));

    }

};
#endif //FEM_SOLVER_GRAVITYGUMMIBEARCONFIG_H
