//
// Created by Wetherbe Admin on 11/5/18.
//

#ifndef FEM_SOLVER_FEMSOLVERCONFIG_H
#define FEM_SOLVER_FEMSOLVERCONFIG_H
#include "../material/Material.h"
#include "../geometry/Geometry.h"
#include "../collision/CollisionHandler.h"
#include "../force/Force.h"


struct FemSolverConfig {

    FemSolverConfig(
        int fps,
        double time_step,
        double yModulus,
        double poisson,
        double density,
        double maxMeshPartVolume
    ):
        fps(fps),
        time_step(time_step),
        maxMeshPartVolume(maxMeshPartVolume),
        material(yModulus, poisson, density)
    {}

    int fps;
    double time_step;

    //material parameters
    Material material;

    double maxMeshPartVolume;

    //geometry parameters
    Matrix<double, 4,4> transform= Matrix<double, 4,4>::Zero();
    Geometry *geometry;
    std::vector<Force<3>*> external_forces;

    virtual void initGeometry() {
        geometry->loadGeometry();
        geometry->maxMeshPartVolume = maxMeshPartVolume;
        geometry->adjustPosition(transform);
        geometry->tetrahedralizeOrigGeometry();
    }
};
#endif //FEM_SOLVER_FEMSOLVERCONFIG_H
