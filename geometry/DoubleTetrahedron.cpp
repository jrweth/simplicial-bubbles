//
// Created by Wetherbe Admin on 10/31/18.
//

#include "DoubleTetrahedron.h"
#include <iostream>
#include <vector>
using std::cout;
DoubleTetrahedron::DoubleTetrahedron() {
    char myFilePrefix[] = {'T', 'e','t','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
}




void DoubleTetrahedron::loadGeometry() { }

void DoubleTetrahedron::tetrahedralizeOrigGeometry() {
}

int DoubleTetrahedron::getNumPoints(){
    return 5;
}
int DoubleTetrahedron::getNumMeshParts(){
    return 2;
}
int DoubleTetrahedron::getNumBoundaryTriangles(){
    return 6;
}
vector<double> DoubleTetrahedron::getPointPosition(int point_index){
    if(point_index == 0)  return {1 , 0,  0};
    if(point_index == 1)  return { 0 , 1,  0};
    if(point_index == 2)  return {0 , 0,  1};
    if(point_index == 3)  return {0 , 0,  0};
    return {0.5 , 1,  0.5};
}
vector<int> DoubleTetrahedron::getMeshElementPoints(int mesh_index){
    if(mesh_index == 0) return {0, 1, 2, 3};
    return {0, 1, 2, 4};
}

array<int, 3> DoubleTetrahedron::getBoundaryTriangle(int triangle_index){

    if(triangle_index == 0) return {0, 1, 3};
    if(triangle_index == 1) return {0, 2, 3};
    if(triangle_index == 2) return {1, 2, 3};

    if(triangle_index == 3) return {0,1,4};
    if(triangle_index == 4) return {2,1,4};
    return {0,2,4};

}