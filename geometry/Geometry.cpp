//
// Created by Wetherbe Admin on 10/19/18.
//

#include <iostream>
#include <sstream>
#include <vector>
#include "Geometry.h"
#include "../tetgen/tetgen.h"

using std::cout;
using std::endl;
using std::vector;

void Geometry::tetrahedralizeOrigGeometry() {

    auto *tetgenb = new tetgenbehavior();
    tetgenb->plc = 1;
    tetgenb->quality = 1;
    tetgenb->minratio = (REAL)1.414;
    tetgenb->fixedvolume = 1;
    tetgenb->maxvolume = (REAL)maxMeshPartVolume;
    tetgenb->coarsen = 1;

    tetrahedralize(tetgenb, &origGeo, &tetGeo);
}

void Geometry::adjustPosition(Matrix<double, 4,4> transformation) {
    for(int i = 0; i < origGeo.numberofpoints; i++) {
        Matrix<double, 4, 1> p;
        p << origGeo.pointlist[i*3], origGeo.pointlist[i*3+1], origGeo.pointlist[i*3 + 2], 1;
        cout << endl<< p.transpose() << " to ";
        p = transformation * p;
        cout << p.transpose();
        origGeo.pointlist[i*3] = p(0,0);
        origGeo.pointlist[i*3 + 1] = p(1,0);
        origGeo.pointlist[i*3 + 2] = p(2,0);
    }
}

void Geometry::getFrameFileName(char* fileName, int frameNumber) {
    int thousand = (int)floor(frameNumber / 1000);
    int hundred = (int)floor(frameNumber / 100) % 10;
    int ten = (int)floor(frameNumber/10) % 10;
    int one = frameNumber % 10;
    std::sprintf(fileName, "%s%d%d%d%d", filePrefix, thousand, hundred, ten, one);
}




int Geometry::getNumPoints(){
    return tetGeo.numberofpoints;
}
int Geometry::getNumMeshParts(){
    return tetGeo.numberoftetrahedra;
}
int Geometry::getNumBoundaryTriangles(){
    return tetGeo.numberoftrifaces;
}

vector<double> Geometry::getPointPosition(int point_index) {
    return {
            tetGeo.pointlist[point_index * 3],
            tetGeo.pointlist[point_index * 3 + 1],
            tetGeo.pointlist[point_index * 3 + 2]
    };
}

vector<int> Geometry::getMeshElementPoints(int mesh_index) {
    return {
            tetGeo.tetrahedronlist[mesh_index * 4] - tetGeo.firstnumber,
            tetGeo.tetrahedronlist[mesh_index * 4 + 1] - tetGeo.firstnumber ,
            tetGeo.tetrahedronlist[mesh_index * 4 + 2] - tetGeo.firstnumber,
            tetGeo.tetrahedronlist[mesh_index * 4 + 3] - tetGeo.firstnumber
    };
}

array<int, 3> Geometry::getBoundaryTriangle(int triangle_index) {
    return {
            tetGeo.trifacelist[triangle_index * 3] -tetGeo.firstnumber,
            tetGeo.trifacelist[triangle_index * 3+ 1] - tetGeo.firstnumber ,
            tetGeo.trifacelist[triangle_index * 3 + 2] - tetGeo.firstnumber
    };
}

