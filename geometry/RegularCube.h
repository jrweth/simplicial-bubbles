//
// Created by Wetherbe Admin on 11/16/18.
//

#ifndef FEM_SOLVER_REGULARCUBE_H
#define FEM_SOLVER_REGULARCUBE_H


#include <vector>
#include "Geometry.h"


class RegularCube: public Geometry {
public:
    RegularCube();
    RegularCube(int divs);

    ~RegularCube() {}

    void loadGeometry();

    void tetrahedralizeOrigGeometry();

    void adjustPosition(Matrix<double, 4, 4> transformation);

    int getNumPoints();

    int getNumMeshParts();

    int getNumBoundaryTriangles();

    int divs;

    vector<vector<vector<int>>> center_points = {};
    vector<vector<vector<int>>> corner_points = {};
    vector<vector<vector<int>>> x_face_points = {};
    vector<vector<vector<int>>> y_face_points = {};
    vector<vector<vector<int>>> z_face_points = {};

    vector<vector<double>> points = {};
    vector<vector<int>> mesh = {};
    vector<array<int, 3>> boundary = {};

    vector<double> getPointPosition(int point_index);

    vector<int> getMeshElementPoints(int mesh_index);

    array<int, 3> getBoundaryTriangle(int triangle_index);


    void createSubCube(array<int, 10> points);
    void addDiv(int x, int y,int z);
    void addMeshPart(int p1, int p2, int p3, int p4);

};
#endif //FEM_SOLVER_REGULARCUBE_H
