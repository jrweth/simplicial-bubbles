//
// Created by Wetherbe Admin on 10/19/18.
//

#ifndef FEM_SOLVER_CUBE_H
#define FEM_SOLVER_CUBE_H
#include "Geometry.h"
#include <vector>


class Cube: public Geometry {
public:
    Cube();
    ~Cube(){}
    void loadGeometry();

};


#endif //FEM_SOLVER_CUBE_H
