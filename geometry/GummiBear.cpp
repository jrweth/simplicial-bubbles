//
// Created by Wetherbe Admin on 11/6/18.
//

#include "GummiBear.h"
#include "../tetgen/tetgen.h"

GummiBear::GummiBear() {
    char myFilePrefix[] = "GummiBear";
    sprintf(filePrefix, "%s", myFilePrefix);
}
void GummiBear::loadGeometry() {
    tetgenmesh();
    char fileName[] = "../data/bear";
    origGeo.load_stl(fileName);
}