//
// Created by Wetherbe Admin on 10/27/18.
//

#include "SingleTetrahedron.h"
#include <iostream>
#include <vector>
using std::cout;
SingleTetrahedron::SingleTetrahedron() {
    char myFilePrefix[] = {'T', 'e','t','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
    points.clear();
    points.push_back({0, 0, 0});
    points.push_back({1, 0, 0});
    points.push_back({0, 1, 0});
    points.push_back({0, 0, 1});
}




void SingleTetrahedron::loadGeometry() { }

void SingleTetrahedron::tetrahedralizeOrigGeometry() {
}

int SingleTetrahedron::getNumPoints(){
    return 4;
}
int SingleTetrahedron::getNumMeshParts(){
    return 1;
}
int SingleTetrahedron::getNumBoundaryTriangles(){
    return 4;
}
vector<double> SingleTetrahedron::getPointPosition(int point_index){
    return points[point_index];
}
vector<int> SingleTetrahedron::getMeshElementPoints(int mesh_index){
    return {0, 1, 2, 3};
}

array<int, 3> SingleTetrahedron::getBoundaryTriangle(int triangle_index){

    if(triangle_index == 0) return {0, 1, 2};
    if(triangle_index == 1) return {2, 1, 3};
    if(triangle_index == 2) return {2, 3, 0};
    return {0, 3, 1};

}

void SingleTetrahedron::adjustPosition(Matrix<double, 4,4> transformation) {
    cout << "adjusting position";
    cout << transformation;

    for(int i = 0; i < points.size(); i++) {
        Matrix<double, 4, 1> p;
        p << points[i][0], points[i][1], points[i][2], 1;
        p = transformation * p;
        points[i][0] = p(0,0);
        points[i][1] = p(1,0);
        points[i][2] = p(2,0);
    }
}