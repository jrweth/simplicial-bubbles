//
// Created by Wetherbe Admin on 10/27/18.
//

#include "Triangle.h"
#include <iostream>
#include <vector>

using std::cout;
Triangle::Triangle() {
    char myFilePrefix[] = {'T', 'r','i','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
}


void Triangle::loadGeometry() { }

void Triangle::tetrahedralizeOrigGeometry() {
}

int Triangle::getNumPoints(){
    return 3;
}
int Triangle::getNumMeshParts(){
    return 1;
}
int Triangle::getNumBoundaryTriangles(){
    return 1;
}
vector<double> Triangle::getPointPosition(int point_index){

    if(point_index == 0)  return vector<double>({-0.5 , -0.5,  0});
    if(point_index == 1)  return vector<double>({0.5 , 0.5,  0});
    if(point_index == 2)  return vector<double>({-0.5 , 0.5,  0});
    return {};
}


vector<int> Triangle::getMeshElementPoints(int mesh_index){
    return {0, 1, 2};
}

array<int, 3> Triangle::getBoundaryTriangle(int triangle_index){

    return {0, 1, 2};

}