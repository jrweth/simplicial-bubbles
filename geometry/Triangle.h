//
// Created by Wetherbe Admin on 10/27/18.
//

#ifndef FEM_SOLVER_TRIANGLE_H
#define FEM_SOLVER_TRIANGLE_H


#include "Geometry.h"

class Triangle: public Geometry {
public:
    Triangle();
    ~Triangle(){}
    void loadGeometry();
    void tetrahedralizeOrigGeometry();

    int getNumPoints();
    int getNumMeshParts();
    int getNumBoundaryTriangles();
    vector<double> getPointPosition(int point_index);
    vector<int> getMeshElementPoints(int mesh_index);
    array<int, 3> getBoundaryTriangle(int triangle_index);
};


#endif //FEM_SOLVER_TRIANGLE_H
