//
// Created by Wetherbe Admin on 10/27/18.
//

#ifndef FEM_SOLVER_SINGLETETRAHEDRON_H
#define FEM_SOLVER_SINGLETETRAHEDRON_H

#include <vector>
#import "Geometry.h"

class SingleTetrahedron: public Geometry{
public:
    SingleTetrahedron();
    ~SingleTetrahedron(){}
    void loadGeometry();
    void tetrahedralizeOrigGeometry();
    void adjustPosition(Matrix<double, 4,4> transformation);

    int getNumPoints();
    int getNumMeshParts();
    int getNumBoundaryTriangles();
    vector<double> getPointPosition(int point_index);
    vector<int> getMeshElementPoints(int mesh_index);
    array<int, 3> getBoundaryTriangle(int triangle_index);
    vector<vector<double>> points = {};
};


#endif //FEM_SOLVER_SINGLETETRAHEDRON_H
