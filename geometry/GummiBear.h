//
// Created by Wetherbe Admin on 11/6/18.
//

#ifndef FEM_SOLVER_GUMMIBEAR_H
#define FEM_SOLVER_GUMMIBEAR_H
#include "Geometry.h"

class GummiBear:public Geometry {
public:
    GummiBear();
    ~GummiBear(){};
    void loadGeometry();
};


#endif //FEM_SOLVER_GUMMIBEAR_H
