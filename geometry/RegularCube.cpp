//
// Created by Wetherbe Admin on 11/16/18.
//

#include "RegularCube.h"

#include "RegularCube.h"
#include <iostream>
#include <vector>
using std::cout;
RegularCube::RegularCube(): divs(1) {
    char myFilePrefix[] = {'T', 'e','t','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
}

RegularCube::RegularCube(int divs): divs(divs) {
    char myFilePrefix[] = {'T', 'e','t','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
}


void RegularCube::adjustPosition(Matrix<double, 4,4> transformation) {
    for(int i = 0; i < points.size(); i++) {
        Matrix<double, 4, 1> p;
        p << points[i][0], points[i][1], points[i][2], 1;
        p = transformation * p;
        points[i][0] = p(0,0);
        points[i][1] = p(1,0);
        points[i][2] = p(2,0);
    }
}
void RegularCube::loadGeometry() {
    double gridSize = 1.0 / (double)divs;
    double halfGridSize = gridSize/ 2.0;

    //add the points
    for(int x = 0; x < divs + 1; x++) {
        corner_points.push_back({});
        center_points.push_back({});
        x_face_points.push_back({});
        y_face_points.push_back({});
        z_face_points.push_back({});
        for(int y = 0; y < divs + 1; y++) {
            corner_points[x].push_back({});
            center_points[x].push_back({});
            x_face_points[x].push_back({});
            y_face_points[x].push_back({});
            z_face_points[x].push_back({});
            for(int z = 0; z < divs + 1; z++) {
                points.push_back({
                    gridSize * x,
                    gridSize * y,
                    gridSize * z
                });
                corner_points[x][y].push_back(points.size()-1);

                if(x < divs && y < divs && z < divs ) {
                    points.push_back({
                        gridSize * x + halfGridSize,
                        gridSize * y + halfGridSize,
                        gridSize * z + halfGridSize
                    });
                    center_points[x][y].push_back(points.size() - 1);
                }

                if(y < divs && z < divs) {
                    points.push_back({
                        gridSize * x,
                        gridSize * y + halfGridSize,
                        gridSize * z + halfGridSize
                    });
                    x_face_points[x][y].push_back(points.size() - 1);
                }

                if(x < divs && z < divs) {
                    points.push_back({
                        gridSize * x + halfGridSize,
                        gridSize * y,
                        gridSize * z + halfGridSize
                    });
                    y_face_points[x][y].push_back(points.size() - 1);
                }

                if(x < divs && y < divs) {
                    points.push_back({
                        gridSize * x + halfGridSize,
                        gridSize * y + halfGridSize,
                        gridSize * z
                    });
                    z_face_points[x][y].push_back(points.size() - 1);
                }

            }
        }
    }

    for(int x = 0; x < divs; x++) {
        for (int y = 0; y < divs; y++) {
            for (int z = 0; z < divs; z++) {
                addDiv(x, y, z);
            }
        }
    }
    cout << "hi";
}

void RegularCube::addDiv(int x, int y, int z) {
    //
    addMeshPart(
       center_points[x][y][z],
       corner_points[x][y][z],
       corner_points[x + 1][y][z],
       z_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z],
        corner_points[x][y+1][z],
        z_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z],
        corner_points[x+1][y+1][z],
        z_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y+1][z],
        corner_points[x+1][y+1][z],
        z_face_points[x][y][z]
    );

    if(z == 0) {
        boundary.push_back({
            corner_points[x][y][z],
            corner_points[x + 1][y][z],
            z_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x][y+1][z],
            corner_points[x][y][z],
            z_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y][z],
            corner_points[x+1][y+1][z],
            z_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y+1][z],
            corner_points[x][y+1][z],
            z_face_points[x][y][z]
        });
    }

    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z+1],
        corner_points[x + 1][y][z+1],
        z_face_points[x][y][z+1]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z+1],
        corner_points[x][y+1][z+1],
        z_face_points[x][y][z+1]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z+1],
        corner_points[x+1][y+1][z+1],
        z_face_points[x][y][z+1]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y+1][z+1],
        corner_points[x+1][y+1][z+1],
        z_face_points[x][y][z+1]
    );

    if(z == divs-1) {
        boundary.push_back({
            corner_points[x + 1][y][z+1],
            corner_points[x][y][z+1],
            z_face_points[x][y][z+1]
        });
        boundary.push_back({
            corner_points[x][y][z+1],
            corner_points[x][y+1][z+1],
            z_face_points[x][y][z+1]
        });
        boundary.push_back({
            corner_points[x+1][y+1][z+1],
            corner_points[x+1][y][z+1],
            z_face_points[x][y][z+1]
        });
        boundary.push_back({
            corner_points[x][y+1][z+1],
            corner_points[x+1][y+1][z+1],
            z_face_points[x][y][z+1]
        });
    }

    //
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z],
        corner_points[x+1][y][z],
        y_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z],
        corner_points[x][y][z+1],
        y_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z+1],
        corner_points[x+1][y][z],
        y_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z+1],
        corner_points[x][y][z+1],
        y_face_points[x][y][z]
    );

    if(y == 0) {
        boundary.push_back({
            corner_points[x][y][z],
            corner_points[x + 1][y][z],
            y_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x][y][z+1],
            corner_points[x][y][z],
            y_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y][z],
            corner_points[x+1][y][z+1],
            y_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y][z+1],
            corner_points[x][y][z+1],
            y_face_points[x][y][z]
        });
    }

    //
    addMeshPart(
            center_points[x][y][z],
            corner_points[x][y+1][z],
            corner_points[x+1][y+1][z],
            y_face_points[x][y+1][z]
    );
    addMeshPart(
            center_points[x][y][z],
            corner_points[x][y+1][z],
            corner_points[x][y+1][z+1],
            y_face_points[x][y+1][z]
    );
    addMeshPart(
            center_points[x][y][z],
            corner_points[x+1][y+1][z+1],
            corner_points[x+1][y+1][z],
            y_face_points[x][y+1][z]
    );
    addMeshPart(
            center_points[x][y][z],
            corner_points[x+1][y+1][z+1],
            corner_points[x][y+1][z+1],
            y_face_points[x][y+1][z]
    );

    if(y == divs -1) {
        boundary.push_back({
            corner_points[x][y+1][z],
            corner_points[x + 1][y+1][z],
            y_face_points[x][y+1][z]
        });
        boundary.push_back({
            corner_points[x][y+1][z+1],
            corner_points[x][y+1][z],
            y_face_points[x][y+1][z]
        });
        boundary.push_back({
            corner_points[x+1][y+1][z],
            corner_points[x+1][y+1][z+1],
            y_face_points[x][y+1][z]
        });
        boundary.push_back({
            corner_points[x+1][y+1][z+1],
            corner_points[x][y+1][z+1],
            y_face_points[x][y+1][z]
        });
    }


    //
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z],
        corner_points[x][y][z+1],
        x_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z],
        corner_points[x][y+1][z],
        x_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y+1][z],
        corner_points[x][y+1][z+1],
        x_face_points[x][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x][y][z+1],
        corner_points[x][y+1][z+1],
        x_face_points[x][y][z]
    );

    if(x == 0) {
        boundary.push_back({
            corner_points[x][y][z],
            corner_points[x][y+1][z],
            x_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x][y][z+1],
            corner_points[x][y][z],
            x_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x][y+1][z],
            corner_points[x][y+1][z+1],
            x_face_points[x][y][z]
        });
        boundary.push_back({
            corner_points[x][y+1][z+1],
            corner_points[x][y][z+1],
            x_face_points[x][y][z]
        });
    }


    //
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z],
        corner_points[x+1][y][z+1],
        x_face_points[x+1][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z],
        corner_points[x+1][y+1][z],
        x_face_points[x+1][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y+1][z],
        corner_points[x+1][y+1][z+1],
        x_face_points[x+1][y][z]
    );
    addMeshPart(
        center_points[x][y][z],
        corner_points[x+1][y][z+1],
        corner_points[x+1][y+1][z+1],
        x_face_points[x+1][y][z]
    );

    if(x == divs -1) {
        boundary.push_back({
            corner_points[x+1][y+1][z],
            corner_points[x+1][y][z],
            x_face_points[x+1][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y][z],
            corner_points[x+1][y][z+1],
            x_face_points[x+1][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y+1][z+1],
            corner_points[x+1][y+1][z],
            x_face_points[x+1][y][z]
        });
        boundary.push_back({
            corner_points[x+1][y][z+1],
            corner_points[x+1][y+1][z+1],
            x_face_points[x+1][y][z]
        });
    }

}

void RegularCube::addMeshPart(int p1, int p2, int p3, int p4) {
    mesh.push_back({p1,p2,p3,p4});

}

void RegularCube::tetrahedralizeOrigGeometry() {
}

int RegularCube::getNumPoints(){
    return points.size();
}
int RegularCube::getNumMeshParts(){
    return mesh.size();
}
int RegularCube::getNumBoundaryTriangles(){
    return boundary.size();
}
vector<double> RegularCube::getPointPosition(int point_index){
    return points[point_index];
}
vector<int> RegularCube::getMeshElementPoints(int mesh_index){
    return mesh[mesh_index];
}

array<int, 3> RegularCube::getBoundaryTriangle(int triangle_index){
    return boundary[triangle_index];


}