//
// Created by Wetherbe Admin on 10/31/18.
//

#ifndef FEM_SOLVER_DOUBLETETRAHEDRON_H
#define FEM_SOLVER_DOUBLETETRAHEDRON_H
#import "Geometry.h"

class DoubleTetrahedron: public Geometry{
public:
    DoubleTetrahedron();
    ~DoubleTetrahedron(){}
    void loadGeometry();
    void tetrahedralizeOrigGeometry();

    int getNumPoints();
    int getNumMeshParts();
    int getNumBoundaryTriangles();
    vector<double> getPointPosition(int point_index);
    vector<int> getMeshElementPoints(int mesh_index);
    array<int, 3> getBoundaryTriangle(int triangle_index);
};


#endif //FEM_SOLVER_DOUBLETETRAHEDRON_H
