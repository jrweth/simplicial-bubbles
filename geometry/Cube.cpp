//
// Created by Wetherbe Admin on 10/19/18.
//

#include "Cube.h"
#include <vector>

Cube::Cube() {
    char myFilePrefix[] = {'C', 'u','b','e','\0'};
    sprintf(filePrefix, "%s", myFilePrefix);
}




void Cube::loadGeometry() {
    int i;
    tetgenio::facet *f;
    tetgenio::polygon *p;

    // All indices start from 1.
    origGeo.firstnumber = 1;

    origGeo.numberofpoints = 8;
    origGeo.pointlist = new REAL[origGeo.numberofpoints * 3];
    origGeo.pointlist[0]  = -1;  // node 1.
    origGeo.pointlist[1]  = -1;
    origGeo.pointlist[2]  = -1;
    origGeo.pointlist[3]  = 1;  // node 2.
    origGeo.pointlist[4]  = -1;
    origGeo.pointlist[5]  = -1;
    origGeo.pointlist[6]  = 1;  // node 3.
    origGeo.pointlist[7]  = 1;
    origGeo.pointlist[8]  = -1;
    origGeo.pointlist[9]  = -1;  // node 4.
    origGeo.pointlist[10] = 1;
    origGeo.pointlist[11] = -1;
    // Set node 5, 6, 7, 8.
    for (i = 4; i < 8; i++) {
        origGeo.pointlist[i * 3]     = origGeo.pointlist[(i - 4) * 3];
        origGeo.pointlist[i * 3 + 1] = origGeo.pointlist[(i - 4) * 3 + 1];
        origGeo.pointlist[i * 3 + 2] = 1;
    }

    origGeo.numberoffacets = 6;
    origGeo.facetlist = new tetgenio::facet[origGeo.numberoffacets];
    origGeo.facetmarkerlist = new int[origGeo.numberoffacets];

    // Facet 1. The leftmost facet.
    f = &origGeo.facetlist[0];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 1;
    p->vertexlist[1] = 2;
    p->vertexlist[2] = 3;
    p->vertexlist[3] = 4;

    // Facet 2. The rightmost facet.
    f = &origGeo.facetlist[1];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 5;
    p->vertexlist[1] = 6;
    p->vertexlist[2] = 7;
    p->vertexlist[3] = 8;

    // Facet 3. The bottom facet.
    f = &origGeo.facetlist[2];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 1;
    p->vertexlist[1] = 5;
    p->vertexlist[2] = 6;
    p->vertexlist[3] = 2;

    // Facet 4. The back facet.
    f = &origGeo.facetlist[3];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 2;
    p->vertexlist[1] = 6;
    p->vertexlist[2] = 7;
    p->vertexlist[3] = 3;

    // Facet 5. The top facet.
    f = &origGeo.facetlist[4];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 3;
    p->vertexlist[1] = 7;
    p->vertexlist[2] = 8;
    p->vertexlist[3] = 4;

    // Facet 6. The front facet.
    f = &origGeo.facetlist[5];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 4;
    p->vertexlist = new int[p->numberofvertices];
    p->vertexlist[0] = 4;
    p->vertexlist[1] = 8;
    p->vertexlist[2] = 5;
    p->vertexlist[3] = 1;

    // Set 'origGeo.facetmarkerlist'

    origGeo.facetmarkerlist[0] = -1;
    origGeo.facetmarkerlist[1] = -2;
    origGeo.facetmarkerlist[2] = 0;
    origGeo.facetmarkerlist[3] = 0;
    origGeo.facetmarkerlist[4] = 0;
    origGeo.facetmarkerlist[5] = 0;
    
    
}