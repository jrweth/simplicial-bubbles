//
// Created by Wetherbe Admin on 10/19/18.
//

#ifndef FEM_SOLVER_GEOMETRY_H
#define FEM_SOLVER_GEOMETRY_H

#import "../tetgen/tetgen.h"
#import "../eigen/Eigen/Dense"
using Eigen::Matrix;
using std::array;
using std::vector;

class Geometry {
public:
    double maxMeshPartVolume = 0.1;
    Geometry(){}
    virtual ~Geometry(){};
    virtual void loadGeometry() = 0;

    tetgenio origGeo;
    tetgenio tetGeo;

    char filePrefix[FILENAMESIZE];
    virtual void tetrahedralizeOrigGeometry();

    void getFrameFileName(char* fileName, int frameNumber);
    virtual void adjustPosition(Matrix<double, 4,4> transformation);

    virtual int getNumPoints();
    virtual int getNumMeshParts();
    virtual int getNumBoundaryTriangles();

    virtual std::vector<double> getPointPosition(int point_index);
    virtual std::vector<int> getMeshElementPoints(int mesh_index);
    virtual array<int, 3> getBoundaryTriangle(int triangle_index);

};


#endif //FEM_SOLVER_GEOMETRY_H
