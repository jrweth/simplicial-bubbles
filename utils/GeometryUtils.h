//
// Created by Wetherbe Admin on 10/20/18.
//

#ifndef FEM_SOLVER_GEOMETRYUTILS_H
#define FEM_SOLVER_GEOMETRYUTILS_H

#import "../eigen/Eigen/Dense"

using namespace Eigen;
using namespace std;
class GeometryUtils {
public:
    static double tetrahedronVolume(array<Matrix<double, 3, 1>, 4> points
    );

    static Matrix<double, 3,3> constructShapeMatrix(array<Matrix<double, 3, 1>, 4>);
    static Matrix<double, 2,2> constructShapeMatrix(array<Matrix<double, 2, 1>, 3>);

};



#endif //FEM_SOLVER_GEOMETRYUTILS_H
