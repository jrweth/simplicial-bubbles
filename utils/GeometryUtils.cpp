//
// Created by Wetherbe Admin on 10/20/18.
//

#include "GeometryUtils.h"
#include <iostream>
using namespace std;
double GeometryUtils::tetrahedronVolume(array<Matrix<double, 3, 1>, 4> points) {

    //create matrix
    Matrix<double, 4, 4> m;
    for(int i = 0; i< 4; i++) {
        for(int j = 0; j <3; j++) {
            m(i, j) = points[i](j,0);
        }
        m(i, 3) = 1.0;
    }

    return abs(m.determinant() / 6.0);


}

Matrix<double, 3,3> GeometryUtils::constructShapeMatrix(array<Matrix<double, 3, 1>, 4> positions) {
    Matrix<double, 3,3> m;
    int meshIndex, i, j;
    for(i = 0; i< 3; i++) {
        for(j=0; j < 3; j++) {
            m(j,i) = positions[i](j,0) - positions[3](j,0);
        }
    }
    return m;
}

Matrix<double, 2,2> GeometryUtils::constructShapeMatrix(array<Matrix<double, 2, 1>, 3> positions) {
    Matrix<double, 2,2> m;
    int meshIndex, i, j;
    for(i = 0; i< 2; i++) {
        for(j=0; j < 2; j++) {
            m(j,i) = positions[i](j,0) - positions[2](j,0);
        }
    }
    return m;
}