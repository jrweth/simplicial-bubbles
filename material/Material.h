//
// Created by Wetherbe Admin on 10/26/18.
//

#ifndef FEM_SOLVER_MATERIAL_H
#define FEM_SOLVER_MATERIAL_H

#include <iostream>
using std::cout;

struct Material {
    Material(double y, double p, double density):
        youngsModulus(y),
        poissonsRatio(p),
        density(density)
    {
        lamda = y*p / ((1.0 + p) * (1.0- 2.0*p));
        mu = y/(2* (1 + p));
        cout << endl << "lamda: " << lamda;
        cout << endl << "mu: " << mu;
    }
    double youngsModulus;
    double poissonsRatio;
    double lamda;
    double mu;
    double density;
};


#endif //FEM_SOLVER_MATERIAL_H
