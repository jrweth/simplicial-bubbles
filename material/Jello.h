//
// Created by Wetherbe Admin on 10/26/18.
//

#ifndef FEM_SOLVER_JELLO_H
#define FEM_SOLVER_JELLO_H

#import "Material.h"
struct Jello: public Material {
    Jello() : Material(80, .4, 1.0) {}
};


#endif //FEM_SOLVER_JELLO_H
