\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
\setlength\parindent{0pt}
\setlength{\parskip}{\baselineskip}%
%SetFonts

%SetFonts


\title{Finite Element Method technique in Computer Graphics}
\author{J. Reuben Wetherbee}
\date{October 16, 2018}							% Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}

One of the persistent problems when modeling deformable elastic bodies for computer graphics is the need to maintain the shape and volume of the original object.  The Finite Element Method borrowed from continuum mechanics is a way to model and apply the elastic energy of the deformed object in order to return it to the undeformed state.  In this paper I will summarize the steps necessary to calculate the elastic force and apply it to the discreet particles of the deformed object. This summary is based heavily off the SIGGRAPH 2012 Course on FEM Simulation of 3D Deformable Solids (http://www.femdefo.org/).


\section{Deformation Gradient $F$}

In order to determine the elastic force of a given object it is first necessary to determine how much the body is being deformed at any given point.  This is done by constructing the deformation gradient.  Consider the function $\overrightarrow \phi$ which deforms all points $\overrightarrow X$ in the undeformed body to its deformed location $\overrightarrow x = \overrightarrow \phi (\overrightarrow X)$.

It is constructed by splitting $\phi$ into three functions for the x, y and z components ($\phi _{1}, \phi_{2}, \phi_{3})$.  Then taking the piecewise derivate .


\[
\begin{bmatrix}
\partial \overrightarrow \phi_{1}/\partial \overrightarrow X_{1} & \partial \overrightarrow \phi_{1}/\partial \overrightarrow X_{2} & \partial \overrightarrow \phi_{1}/\partial \overrightarrow X_{3} \\
\partial \overrightarrow \phi_{2}/\partial \overrightarrow X_{1} & \partial \overrightarrow \phi_{2}/\partial \overrightarrow X_{2} & \partial \overrightarrow \phi_{3}/\partial \overrightarrow X_{3} \\
\partial \overrightarrow \phi_{2}/\partial \overrightarrow X_{1} & \partial \overrightarrow \phi_{2}/\partial \overrightarrow X_{2} & \partial \overrightarrow \phi_{3}/\partial \overrightarrow X_{3}
\end{bmatrix}
\]


\section{Strain Energy: Energy Density Function $\Psi$}

The deformation gradient describes how each point has been deformed, but it does not determine how much energy is produced by this deformation.  Different materials react differently to deformation.  When dealing with elastic bodies, it is immaterial how the body reached it's deformed state, but only the relation of the deformed body to it's undeformed state and how the material reacts.  This is represented by an energy density function $\Psi$ which measures the strain energy over the undeformed volume for a particular point $\overrightarrow X$. The total energy for the deformation function on the body would then be represented as

\[
E[\phi] = \int_{\Omega}\Psi[\phi;\overrightarrow X]\partial\overrightarrow X
\]
Where this really starts paying off is that for a particular location $\overrightarrow X_{*}$ the relationship can be expressed in terms of the deformation gradient at that location $F_{*}$.  This can be shown by using the Taylor expansion and the fact that in the infinitesimally small area around $\overrightarrow X_{*}$ the translation term can be ignored.  What we then end up with is that $\Psi$ can be represented as a function of $F$.
\begin{gather*}
   \Psi[\phi,\overrightarrow X]=\Psi(F)
\end{gather*}

The challenge is then to come up with an energy function $\Psi(F)$ which accurately represents the energy density function of a material.

\section{Stress Tensor $P$}

Because the interior and exterior points in a deforming body behave differently, to get a unified model for both points it is necessary to construct a stress tensor.  For this case we can use the 1st Piola-Kirchhoff stress tensor which is a 3 X 3 matrix which represents the traction at boundary conditions as well as the interior force density.  This works especially well since as part of our process of discretization we will be using nodes and $P$ can be constructed for each node.

For hyperelastic solids this can be written as function of the deformation gradient.
\begin{gather*}
P(F) = \partial\Psi (F) / \partial F
\end{gather*}
Therefore the Piola-Kirchoff stress tensor can be used to obtain force and tension at particular nodes and can be computed using $\Psi$.


\section{Strain Measures}

The crux of the issue is to construct a strain measure which can accurately use the deformation gradient $F$ to determine just how much the body has been deformed.

\subsection{Simple Strain Measure}
To take a simplistic example you could say that you desire your deformation to be very small (equal to $I$ so you could take as your strain measure the Frobenius norm of $F-I$.

\begin{gather*}
E=\vert\vert F \vert\vert _{F}^{2}
\end{gather*}

It is easy to see several issues with this strain measure.  The principal problem is that it is not rotationally invariant.  A pure rotation(or translation) should not cause any elastic energy.  But using $\vert\vert F-I \vert\vert$ would cause the energy to be non zero.  Since a body which has just been rotated or translated should cause no elastic energy to be created this measure has limited use.

\subsection{Green Strain Tensor}
A better approximation and way to get a rotationally invariant strain measure is the Green Strain Tensor.
\begin{gather*}
E= \frac{1}{2}(F^{T}F - I)
\end{gather*}

In this case $E=0$ where when the deformation function is a rotation and/or translation $\overrightarrow \phi (\overrightarrow X) = R(\overrightarrow X) + \overrightarrow t$ so we achieve translational and rotational invariance.  The biggest problem with this model of energy is that you get a quadratic function of deformation which can be costly in computation.

\subsection{Other Strain Tensors}
\begin{itemize}
  \item linear elastiticity using small strain tensor
  \item St. Venant-Kirchoff method
  \item Corotated Linear elasticity
\end{itemize}

  These methods have various advantages and disadvantages for both computation and how they deform over various forces.

\subsection{Neohookian Strain Tensor}
Neohookian elasticity is an excellent model for determining the strain from the deformation gradient because it is both rotationally invarrient and also resists compression.  The energy density is defined by:

\[
\Psi (F) = \frac{\mu}{2}(I_{1} - 3) - \mu \text{log}J + \frac{\lambda}{2} \text{log}^2(J)
\]
Where 
\begin{itemize}
  \item $I_{3} = trace(F^{T}F)$
  \item $J = \text{det} F$
  \item $\mu$ Young's modulus
  \item $\lambda $ Lam\'e coefficient representing incompressibility
\end{itemize}

The more the element is compressed the higher the elastic energy resisting it due to the $\text{log}^2(J)$ term.  As J (which captures volume change) approaches 0 this term increases insuring that the volume does not collapse.

The Stress tensor associated with Neohookian materials is:
\[
P(F) = \mu (F - \mu F^{-T}) + \lambda \text{log}(J)F^{-T} 
\]
\section{Finite Element Method}

The finite element method is a process brought over from continuum mechanics that will

\subsection{Discretization of Force}
The first task is to find a way to discretize the body so that deformation gradient and elastic energy functions can be created
So how do we take the notion of the deformation gradient $F$, the stress tensor and the force and calculate the forces on discreet points within our body.  Since the total energy of a deformation function over the body can be defined in the following way:
\begin{gather*}
E[\phi] := \int_{\Omega}\Psi(F)\partial\overrightarrow X
\end{gather*}

The discrete energy at each individual point would then be for the given degrees of freedom x (namely x,y,z) can be then written as
\begin{gather*}
\overrightarrow f_{i}(x) = - \frac{\partial E(x)}{\partial \overrightarrow x_{i}}
\end{gather*}

This can be written as
\begin{gather*}
\overrightarrow f := (\overrightarrow f_{1}, \overrightarrow f_{2}, ..., \overrightarrow f_{N}) = - \frac{\partial E(x)}{\partial \overrightarrow x_{i}}
\end{gather*}


\subsection{Using Tetrahedral Mesh for Discretization}
Tetrahedral meshes work extremely well for discretization of the deforming body. One reason for this is that as all points of the tertrahedron occupy a similar place in the original mesh we can use the same deformation gradient for all of the vertex.  Also, since we do not care about translation in defining our deformation, we can use 1 vertex ($\overrightarrow X_{4}$) as a reference point and set the deformation of the other points in reference to that one point.  Therefore we can get a shape matrix for both the undeformed state $D_{m}$ and the deformed state $D_{s}$ where:





\[
D_{m} =
\begin{bmatrix}
X_{1} - X_{4} & X_{2} - X{4} & X_{3} - X_{4}\\
Y_{1} - Y_{4} & Y_{2} - Y{4} & Y_{3} - Y_{4}\\
Z_{1} - Z_{4} & Z_{2} - Z{4} & Z_{3} - Z_{4}
\end{bmatrix}
\]
\[
D_{s} =
\begin{bmatrix}
x_{1} - x_{4} & x_{2} - x{4} & x_{3} - x_{4}\\
y_{1} - y_{4} & y_{2} - y{4} & y_{3} - y_{4}\\
z_{1} - z_{4} & z_{2} - z{4} & z_{3} - z_{4}
\end{bmatrix}
\]


These matricies are exteremely useful in our discretization.  First we can easily calculate the volume of the undeformed tetrahedron as
\[
W = \frac{1}{6}|\text{det}D_{m}|
\]
 can also very easily compute F for each deformed tetrahedron by:
\[
 F=D_{s}D_{m}^{-1}
\]
Since $D_{m}^{-1}$ and it is nonsingular for tetrahedron with non-zero volume we can precompute this value so that determining the inverse will only have to be performed once.

Finally, we can very easily compute the elastic force on each vertex of the tertrahedron by using the Piola stress which is defined by our material definition $\Psi$.

\[
H = [\overrightarrow f_{1} \overrightarrow f_{2} \overrightarrow f_{3}] = -WP(F)D_{m}^{-T} \text{   and    } \overrightarrow f_{4} = -\overrightarrow f_{1} -\overrightarrow f_{2} -\overrightarrow f_{3}
\]

This is all that is necessary to use Forward Euler to compute the explicit time integration scheme.

\subsection{Force Differentials for Forward Euler}

In order to achieve a Backward Euler scheme it is necessary to construct force differentials in order to properly calculate $x^{n+1}$ and $v^{n+1}$ for each point in our mesh.  Just as the elastic force was evaluated for each vertex in each tetrahedron, we now need to evaluate the elastic force differential for each vertex in each tetrahedron.  As before we can represent the first force differentials into a matrix $\delta H$ which represents $(\delta \overrightarrow f_{1}, \delta \overrightarrow f_{2}, \delta \overrightarrow f_{3})$.

\[
\delta H = 
\begin{bmatrix}
(\delta \overrightarrow f_{1} & \delta \overrightarrow f_{2} & \delta \overrightarrow f_{3}
\end{bmatrix}
\]

These matrix can be obtained by taking differentials on the definition of $H$.
\[
\delta H = -W\delta P(F; \delta F)D_{m}^{-T}
\]

To evaluate this we can compute $\delta F$ by taking the differentials on F.
\[\delta F = (\delta D_{s})D_{m}^{-1}\]

Where
\[
\delta D_{s} := 
\begin{bmatrix}
\delta x_{1}  - \delta x_{4} && \delta x_{2} - \delta x_{4} && \delta x_{3} - \delta x_{4} \\
\delta y_{1}  - \delta y_{4} && \delta y_{2} - \delta y_{4} && \delta y_{3} - \delta y_{4} \\
\delta z_{1}  - \delta z_{4} && \delta z_{2} - \delta z_{4} && \delta z_{3} - \delta z_{4} 
\end{bmatrix}
\]

And using the Neohookean material definition of for our stress we get:
\[
\delta P(F;\delta F) = \mu \delta F + [\mu - \lambda \text{log}(J)]F^{-T}\delta F^{T}F^{-T} + \lambda \text{tr}(F^{-1}\delta F)F^{-T}
\]

This is all we need to compute the elastic force differentials to use for Backwards Euler integration.

\subsection{Basic Computations used in Backwards Euler Scheme}

\begin{itemize}
   \item Precomputations
   \begin{itemize}
       \item Discretize object into tetrahedra
       \item Foreach Tetrahedron $T$  precompute  $D_{m}$, $D_{m}^{-1}$, and $W$
    \end{itemize}
    \item Computing elastic force
    \begin{itemize}
        \item using $\overrightarrow x$ compute $D_{s}, F, P, H$ to obtain elastic force for each vertex $f_{1}, f_{2}, f_{3}, f_{4}$
    \end{itemize}
    \item Computing elastic force differential
    \begin{itemize}
        \item Compute $\delta D_{s}, \delta F, \delta P, \delta H$ to obtain force differential for each vertex $\delta f_{1}, \delta f_{2}, \delta f_{3}, \delta f_{4}$ 
    \end{itemize}
\end{itemize}

\subsection {Iterative Process to find $x^{n+1}$ and $v^{n+1}$}
The goal in Backward Euler is to solve for the position and velocity of each point after a discrete time step in the following way:
\[
x^{n+1} = x^{n} + \Delta tv^{n+1}
\]
\[
v^{n+1} = v^{n} + \Delta tM^{-1}f(x^{n+1},v^{n+1})
\]

To determine these unknowns we can use an iterative process to approximate $x^{n+1}$ and $v^{n+1}$. Where the first iteration is derived from the current position and velocity.
\[
x_{(k)}^{n+1} \xrightarrow{k\xrightarrow{}\infty} x^{n+1} , v_{(k)}^{n+1} \xrightarrow{k\xrightarrow{}\infty} v^{n+1}
\]
We also can define the change of the position and change of the velocity at each iteration as the following:
\[
\Delta x_{(k)} = x_{(k+1)}^{n+1} - x_{(k)}^{n+1} \text{ and } \Delta v_{(k)} = v_{(k+1)}^{n+1} - v_{(k)}^{n+1} 
\]
Since $v_{(k+1)}^{n+1}$ can be computed directly from $v_{(k)}^{n+1}$ and $\Delta x$ the action in each step of the iteration is to solve for $\Delta x$. This system of equations can be represented by the following:

\[
[(1 + \frac{\gamma}{\Delta t})K(x_{k}^{n+1}) + \frac{1}{\Delta t^{2}}M]\Delta x = \frac{1}{\Delta t}M(v^{n}-v_{k}^{n+1}) + f(x_{k}^{n+1}, v_{k}^{n+1})
\]

Where $M$ is the lumped Mass matrix, $K(x^{*}) = - \frac{\partial f_{e}}{\partial x}$  (the elasticity stiffness matrix around $x^{*}$), and $\gamma$ is the parameter representing the damping forces according to the Raleigh damping method.
\par
The values for $\Delta x$ and $\Delta v$ are used to create $x_{k+1}^{n+1}$ and $v_{k+1}^{n+1}$ and the iteration is run until $\Delta x$ and $\Delta v$ reach an acceptable level.






\end{document}
