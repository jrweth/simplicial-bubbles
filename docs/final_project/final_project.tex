\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage[left=1cm, right=1cm, top=1cm]{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\setlength{\columnseprule}{0.4pt}
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ...
%\geometry{landscape}                		% Activate for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{multicol}
\setlength\parindent{0pt}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{subfig}
\usepackage{xcolor}
\definecolor{light-gray}{gray}{0.95}
\newcommand{\code}[1]{\colorbox{light-gray}{\footnotesize{\texttt{#1}}}}
\setlength{\parskip}{\baselineskip}%
%SetFonts

%SetFonts


\title{Finite Element Method simulation of Jello Cube with Collisions}
\author{J. Reuben Wetherbee}
\date{December 1st, 2018}							% Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}

This paper will explore the background and implementation for creating a visual simulation of hyper-elastic material using the Finite Element Method (FEM) from continuum mechanics.  It will also demostrate the implementation of collisions of the hyper-elastic materials with both rigid bodies (ground plane, staircase, sphere) and other hyper-elastic objects. The simulation presented here is written in C++ using both the Eigen and TETGEN libraries.  The simulation outputs simple .poly files for each frame which are imported into the Houdini 3D animation tool for rendering. This simulation is an implementation of the meterials presented in the SIGGRAPH 2012 Course on FEM Simulation of 3D Deformable Solids (http://www.femdefo.org/).  Code for the simulation can be found at https://bitbucket.org/jrweth/simplicial-bubbles/src/master/ .


\section{Tetrahedralization of Geometry using TETGEN}
In order to use FEM for 3D elastic materials, the base geometries of the objects in question must first be deconstructed into a tetrahedral mesh.  This is accomplished using the TETGEN open source libaray (http://TETGEN.org).  TETGEN is advantageous because it can accept an .stl file as the basis for the geometry tetrahedralization so this method can be used for most geometries which can be converted to an .stl file.  Importing an .stl file is used for some of the alternate geometries presented below (gummy bear).  However, since the cube was the main subject of the simulation, the base geometry for tetrahedralization by TETGEN is constructed directly in C++ as a Tetgenio object which is the data structure required by TETGEN.

One particular benefit of using TETGEN pertinent to the simulation is that it is possible to specify the maximum tetrahedron volume for the tetrahedralization.  This was useful for creating the appropriate granularity of the meshes while testing and tuning the simulation parameters.

\begin{figure}
  \center
  \includegraphics[width=5cm]{tetrahedralized_cube.png}
  \caption{Cube Tetrahedralized by TETGEN}
  \label{fig:tetcube}
\end{figure}

\section{Finite Element Method}

The basic idea of the Finite Element method is to discretize the elastic object into smaller and smaller tetrahedra.  Under deformation of the object, each of the constituent tetrahedra attempt to maintain their original shape, creating an elastic force on each vertex.  The application of sum of all the vertex forces serve to bring the object back into its undeformed state.


\subsection{Using Tetrahedral Mesh for Discretization}
The foundation of computing the elastic forces created by each tetrahedra is to compute the deformation gradient $F$ for each tetrahedra.   Since translation should be ignored in defining our deformation, we can use 1 vertex ($\overrightarrow X_{4}$) as a reference point and set the deformation of the other points in reference to that one point.  Therefore a shape matrix is constructed for both the undeformed state ($D_{m}$) at $t=0$ and the deformed state ($D_{s}$) at $t=t_{n}$  where:

\begin{equation}
D_{m} =
\begin{bmatrix}
X_{1} - X_{4} & X_{2} - X{4} & X_{3} - X_{4}\\
Y_{1} - Y_{4} & Y_{2} - Y{4} & Y_{3} - Y_{4}\\
Z_{1} - Z_{4} & Z_{2} - Z{4} & Z_{3} - Z_{4}
\end{bmatrix}
\end{equation}

\begin{equation}
D_{s} =
\begin{bmatrix}
x_{1} - x_{4} & x_{2} - x{4} & x_{3} - x_{4}\\
y_{1} - y_{4} & y_{2} - y{4} & y_{3} - y_{4}\\
z_{1} - z_{4} & z_{2} - z{4} & z_{3} - z_{4}
\end{bmatrix}
\end{equation}


These matricies are used for the basis for the discretization in the simulation.  The volume of the undeformed tetrahedron is computed as:
\begin{equation}
W = \frac{1}{6}|\text{det}D_{m}|
\end{equation}
 The deformation gradient $F$ is computed for each deformed tetrahedron by:
\begin{equation}
 F=D_{s}D_{m}^{-1}
\end{equation}
Since $D_{m}^{-1}$ is nonsingular for tetrahedron with non-zero volume $D_{m}^{-1}$ is precomputed so that determining the inverse will only have to be performed once.

Finally, the elastic force $f_{i}$ on each vertex of the tertrahedron is computed by using the Piola stress $P$ which is defined by our material definition $\Psi$.

\begin{equation}
H = [\overrightarrow f_{1} \overrightarrow f_{2} \overrightarrow f_{3}] = -WP(F)D_{m}^{-T}
\end{equation}
and
\begin{equation}
\overrightarrow f_{4} = -\overrightarrow f_{1} -\overrightarrow f_{2} -\overrightarrow f_{3}
\end{equation}


\subsection{Finding $P$: Neohookian Strain Tensor}
Neohookian elasticity is an excellent model for determining the strain from the deformation gradient because it is both rotationally invarrient and also resists compression.  The energy density is defined by:

\begin{equation}
\Psi (F) = \frac{\mu}{2}(I_{1} - 3) - \mu \text{log}J + \frac{\lambda}{2} \text{log}^2(J)
\end{equation}
Where
\begin{itemize}
  \itemsep0em
  \item $I_{3} = trace(F^{T}F)$
  \item $J = \text{det} F$
  \item $\lambda $ Lam\'e first parameter representing incompressibility
  \item $\mu$ Lam\'e second parameter representing shear modulus
\end{itemize}

The more the element is compressed the higher the elastic energy resisting it due to the $\text{log}^2(J)$ term.  As J (which captures volume change) approaches 0 this term increases insuring that the volume does not collapse.

The associated stress tensor $P$ associated with the Neohookian model used to determine the elastic forces is:
\begin{equation}
P(F) = \mu (F - F^{-T}) + \lambda \text{log}(J)F^{-T}
\end{equation}


\section{FEM Implementation}

Using the concepts listed above the FEM implementation is created using C++.

\subsection{Basic Algorithm}

Precompute Steps:
\begin{itemize}
  \itemsep0em
  \item Tetrahedralize Geometry using TETGEN
  \item Initialize position ($pos$) and velocity ($vel$) for each point
  \item Initialize shape matrix ($D_{m}$), shape matrix inverse ($D_{m}^{-1}$) and mass ($W$) for each tetrahedra
\end{itemize}

Steps for each time step for each tetrahedra in the mesh
\begin{itemize}
  \itemsep0em
  \item Compute deformed shape matrix $D_{s}$ from equation(2)
  \item Compute deformation gradient $F$ from equation (4)
  \item Compute stress $P$) from equation (8)
  \item Compute $H$ from equation (5)
  \item Compute $f1$, $f2$, $f3$, $f4$ for each point in the tetrahedra from equation (6)
  \item sum the forces ($f$) from all tetrahedra to get the elastic force for each point
  \item add any external forces for each point
  \item adjust the velocity of each point given the previous velocity, force and time step
  \item adjust the position given of each point given previous position and velocity
  \item adjust position/velocity due to any collisions
\end{itemize}


\subsection{Oscillation Adjustment}
Once the initial FEM Solver was implemented it was clear that although the cube maintained its basic shape after a stretching deformation, the elastic forces were continuing to act.  On some tetrahedra it could be observed that the elastic forces were even increasing as the simulation time went forward.  By simplifying the simulation to use a single tetrahedra, it could be clearly observed that the forces were working but the single tetrahedron began to oscillate between positions as the 4 vertex continually compensated in an attempt to return to the original shape.  To address this problem, a viscosity/dampening force was applied at each time step opposite to the velocity created by the elastic forces on each vertex.   This allowed the material to gradually come to rest.

 From the simulation (see figures 2 through 4) it is clear that as the cube underwent stress, the basic shape was maintained and returned to it's rest form by the elastic forces.


\begin{figure}[!htb]
\center
\begin{minipage}{.3\textwidth}
   \includegraphics[width=5cm]{stretched_cube.png}
   \caption{Stretched Cube 1}
   \label{fig:stertechcube}
\end{minipage}
\begin{minipage}{.3\textwidth}
    \includegraphics[width=5cm]{stretched_cube2.png}
    \caption{Stretched Cube 2 }
    \label{fig:stretchcube2}
\end{minipage}
\begin{minipage}{.3\textwidth}
     \includegraphics[width=5cm]{stretched_cube3.png}
     \caption{Stretched Cube 3}
     \label{fig:stretchcube3}
\end{minipage}
\end{figure}


\subsection{Tetrahedron Collapse or Inversion}
Although the dampening term helped with oscillation, the most critical issue with the FEM Solver occurred when a single tetrahedron would either become extremely compressed or even inverted. The $log(J)$ term from the neohookian stress equestion term would explode or completely collapse/invert and the pressure would either explode or become unsolvable. This became apparent with the introduction of gravity and collision with the ground plain.  Several solutions were added to help with this problem.

\subsubsection{Instabilty Improvement:\\Change to Co-rotated Material Model}

In order to avoid the problem of the log(J) term for highly compressed or inverted tetrahedra, a co-rotated material model was used to compute the elastic forces.  This was applied by checking the J term (deformation determinant) and if it was below a certain threshold, the elastic forces were calculated using the fixed co-rotated elasticity material model.  This did keep the simulation from failing for values of $J <= 0$, but for certain parameter values the simulation would still cause the elastic force to become unstable.

\subsubsection{Instabilty Improvement:\\Creation of Regular Tetrahedral Mesh}
Another issue contributing to the explosion of the elastic forces was the irregularity of the tetrahedral mesh created by TETGEN.  If two tetrahedra of very different sizes shared a vertex, the forces acting on the larger tetrahedra would have a massive effect on the neighboring smaller tetrahedra.  Therefore it was necessary to bypass TETGEN and manually construct the tetrahedral mesh for the cube.  Once the process for constructing one cube shaped mesh (using 24 tetrahedra) was created, this process could be repeated to create finer or coarser a tetrahedral cube meshes.

\begin{figure}[!htb]
\center
   \includegraphics[width=5cm]{regular_cube.png}
   \caption{Cube with Regular Tetrahedra}
   \label{fig:regulartetrahedra}
\end{figure}

\subsubsection{Instabilty Improvement:\\Parameter Tuning}



Parameter tuning was also useful for keeping the simulation from instability.  Adjusting the parameters for Young’s Modulus, Poisson’s ratio,  the viscosity/dampening field and the external forces would often allow the simulation to react correctly to the deformation stress without instability.

\subsubsection{Instability Improvement:\\Decrease Time Step}
Although all of the above techninques were useful for solving the instability problem, the most effective solution was to decrease the time step between iterations.  However, depending on the number of tetrahedron, this can only be taken so far until simulation run time constraints become an issue.

\subsection{Alternate Geometries}
Because of the limitations of TETGEN additional geometries that were more complex than the cube were difficult to simulate without instability.  This was generally caused by the varying sizes of tetrahedra around sharp corners of the tetrahedralized objects.  In this case this was solved by creating a geometry (Gummy Bear) that had a simplified geometry that could be properly divided by TETGEN.

\begin{figure}[!htb]
\center
   \includegraphics[width=5cm]{gummy_bear.png}
   \caption{TETGEN of gummy bear}
   \label{fig:gummy bear}
\end{figure}

\section{Collision Implementations}
The following collisions were incorporated into the simulation to model how the materials react to different collision types.


\subsection{Ground Plane}
The ground plane is simply implemented by checking the y position of each point of the mesh.  If position is below the floor plane ($y=0$) then both the velocity and y-position of the point was set to 0.

\begin{lstlisting}[backgroundcolor = \color{lightgray}]
if(point.position.y > 0)
    point.position.y = 0;
    point.velocity = 0;
\end{lstlisting}

\begin{figure}[!htb]
\center
   \includegraphics[width=5cm]{cube_ground.png}
   \caption{ground plane and gravity}
   \label{fig:groundgravity}
\end{figure}

\subsection{Staircase}
The staircase collision is slightly more complicated.   To determine if the point is beneath the staircase is a simple check:

\begin{lstlisting}[backgroundcolor = \color{lightgray}]
if(y < floor(x) + stairHeight)
\end{lstlisting}

To move the position out of the inside of the staircase it is necessary to find out if the point is closer to the “rise” or “run” of the staircase and therefore which of the x or y coordinates of the point must be adjusted.

\begin{lstlisting}[backgroundcolor = \color{lightgray}]
if(ceil(y) > x - floor(x))
  x = floor(x);
else
  y = floor(x) + height;
\end{lstlisting}

One edge case that also must be considered is if both the x and y coordinates must be adjusted:
\begin{lstlisting}[backgroundcolor = \color{lightgray}]
if(y < floor(x) + height - 1)
  x = floor(x)
  y = floor(x) + height - 1
\end{lstlisting}


\subsection{Moving Sphere}
Collision with a moving sphere is also implemented in the simulation.  This is accomplished by moving the center of the sphere along a trajectory at each time step.  A point is determined to be in the sphere by checking that the distance from the center is less than the radius of the sphere.  If it is then the position of the point is adjusted to the closest point on the sphere.

\begin{lstlisting}[backgroundcolor = \color{lightgray}]
diff = pt.postion - sphere.position
if(||diff|| < radius)
  pt.position = pt.position + diff * radius / ||diff||
\end{lstlisting}

An issue that occurs with collision with the moving sphere is that if the parameters are not tuned correctly or the mesh is not of sufficient density, then the sphere penetrates into the interior of the cube.  This can be mitigated by tuning parameters, increasing the density of the tetrahedral mesh, and decreasing the time step.

\subsection{Collision with other simulated objects}

Collision with other simulated objects is significantly more complicated since the position of each object is not predetermined at each step.  This simulation utilizes the fact the simulation already stores the boundary points and triangles of each object.  Checking for collisions between all the tetrahedra of each object with all the tetrahedra of each other object would be very costly.  The simulation simplifies this problem by only checking for collisions between boundary tetrahedra of each object.  A boundary tetrahedra is defined as any tetrahedra which has at least one point on the boundary of the object.

\subsubsection{Collision detection algorithm}
At each time step the following checks are performed to detect collisions between objects: A and B.
\begin{enumerate}
  \itemsep0em
\item Bounding Box for object A checked for overlap with bounding box of B.
\item Boundary Points for object A checked if in the interior of bounding box of each boundary tetrahedron of B.
\item Boundary Points for object A checked if in the interior of boundary tetrahedron B
\end{enumerate}

The  third step was performed by checking that for each plane of tetrahedra B, the boundary point of A is in the half-space on the interior side of that plane. If this holds true for all planes of Tetrahedra B then point A is in the interior.  The check if point A was in the half-space on the interior of the plane B was done by the following:

\begin{enumerate}
  \itemsep0em
    \item Find the normal to plane B by picking 1 point and perform the cross product on the two edges of the plane that meet at that point:
        \begin{lstlisting}[backgroundcolor = \color{lightgray}]
normal = (planeB.point1 - planeB.point2) X (planeB.point3 - planeB.point1)
           \end{lstlisting}

    \item Use the dot product to determine that A is on the interior half-space.
        \begin{lstlisting}[backgroundcolor = \color{lightgray}]
if(normal dot (pointA - planeB.point1) < 0)
           \end{lstlisting}

\end{enumerate}
\subsubsection{Adjusting of identified collision points}

Once the boundary point of A has been identified as being in the interior of boundary tetrahedra B it is necessary to adjust the position of point A outside the boundary of object B.  There are three possible scenarios depending on which type of tetrahedron the point from object A has penetrated.

\begin{itemize}
  \itemsep0em
\item If tetrahedra B only has one point on the boundary then move point A to that boundary point.
\item If tetrahedra B has two points on the boundary then move point A to the closest point on the boundary line defined by those two points
\item If tetrahedra B has 3 or more points on the boundary then move point A to the closest point on the boundary plane (or planes) of tetrahedra B
\end{itemize}




\end{document}
	