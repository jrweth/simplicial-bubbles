//
// Created by Wetherbe Admin on 10/27/18.
//

#ifndef FEM_SOLVER_PUSH_H
#define FEM_SOLVER_PUSH_H
#include "Force.h"
#include <iostream>
using std::cout;
using std::endl;
template <int dim>
class Push: public Force<dim> {
public:
    double x, y, z;
    Push(double x, double y, double z);
    Push(double x, double y);
    Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex);
};


template<int dim>
Push<dim>::Push(double x, double y, double z): x(x), y(y), z(z) { };

template<int dim>
Push<dim>::Push(double x, double y): x(x), y(y) { };

template<int dim>
Matrix<double, dim, 1> Push<dim>::calculate(FemSolver<dim> *fem, int pIndex) {
    Matrix<double, dim, 1> force;
    if(dim == 3) force << x, y, z;
    if(dim == 2) force << x, y ;

    return force * fem->point_mass[pIndex];

};


#endif //FEM_SOLVER_PUSH_H
