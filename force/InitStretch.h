//
// Created by Wetherbe Admin on 11/3/18.
//

#ifndef FEM_SOLVER_INITSTRETCH_H
#define FEM_SOLVER_INITSTRETCH_H

#include "Force.h"
#include "../FemSolver.h"

template<int dim>
class InitStretch: public Force<dim> {
public:
    InitStretch();
    InitStretch(double force);
    Matrix<double, dim, 1>  stretchForce; //from 0 to 1 how much damping you want on velocity
    Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex);
};

template<int dim>
InitStretch<dim>::InitStretch() {
    stretchForce(0,0) = 10;
}

template<int dim>
InitStretch<dim>::InitStretch(double force) {
    stretchForce(0,0) = force;
}

template<int dim>
Matrix<double, dim, 1> InitStretch<dim>::calculate(FemSolver<dim> *fem, int pIndex) {
    return stretchForce * fem->point_mass[pIndex] * fem->position[pIndex](0,0) / fem->current_frame;
};
#endif //FEM_SOLVER_INITSTRETCH_H
