//
// Created by Wetherbe Admin on 10/20/18.
//

#ifndef FEM_SOLVER_GRAVITY_H
#define FEM_SOLVER_GRAVITY_H
#import "Force.h"
#import "../eigen/Eigen/Dense"
#include "../FemSolver.h"

using namespace Eigen;

template<int dim>
class Gravity: public Force<dim> {
public:
    double gravityScale = 1.0;
    Gravity<dim>(){};
    Gravity<dim>(double gravityScale);
    Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex);
};

template<int dim>
Gravity<dim>::Gravity(double gravityScale) {
    this->gravityScale = gravityScale;
}

template<int dim>
Matrix<double, dim, 1> Gravity<dim>::calculate(FemSolver<dim> *fem, int pIndex ){
    Matrix<double, dim, 1> force;
    force << 0, -9.8 * fem->point_mass[pIndex] * gravityScale, 0;

    return force;
}

#endif //FEM_SOLVER_GRAVITY_H