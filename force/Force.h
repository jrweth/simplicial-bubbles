//
// Created by Wetherbe Admin on 10/20/18.
//

#ifndef FEM_SOLVER_FORCE_H
#define FEM_SOLVER_FORCE_H
#import "../eigen/Eigen/Dense"

using namespace Eigen;


template<int dim>
class FemSolver;

template<int dim>
class Force {
public:
    Force() = default;
    ~Force() = default;
    virtual Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex) =0;
    virtual Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex, Matrix<double, dim, 1> pointForce);
};

template<int dim>
Matrix<double, dim, 1> Force<dim>::calculate(FemSolver<dim> *fem, int pIndex, Matrix<double, dim, 1> pointForce) {
    return calculate(fem, pIndex);
}
#endif //FEM_SOLVER_FORCE_H
