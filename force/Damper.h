//
// Created by Wetherbe Admin on 10/27/18.
//

#ifndef FEM_SOLVER_DAMPER_H
#define FEM_SOLVER_DAMPER_H
#include "Force.h"

template<int dim>
class Damper: public Force<dim> {
public:
    Damper(){};
    Damper(double damping);
    double damping = 0.5; //from 0 to 1 how much damping you want on velocity
    Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex);
    Matrix<double, dim, 1> calculate(FemSolver<dim> *fem, int pIndex, Matrix<double, dim, 1> pointForce);
};

template<int dim>
Damper<dim>::Damper(double damping) {
    this->damping = damping;
}

template<int dim>
Matrix<double, dim, 1> Damper<dim>::calculate(FemSolver<dim> *fem, int pIndex) {
   return -fem->velocity[pIndex] * damping * fem->point_mass[pIndex];
};

template<int dim>
Matrix<double, dim, 1> Damper<dim>::calculate(FemSolver<dim> *fem, int pIndex, Matrix<double, dim, 1> pointForce) {


    double dot = fem->velocity[pIndex].dot(fem->velocity[pIndex]);
    return -fem->velocity[pIndex] * damping * fem->point_mass[pIndex];// * abs(dot);
}

#endif //FEM_SOLVER_DAMPER_H
