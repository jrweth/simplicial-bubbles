//
// Created by Wetherbe Admin on 11/6/18.
//

#ifndef FEM_SOLVER_COLLISIONHANDLER_H
#define FEM_SOLVER_COLLISIONHANDLER_H


#include "../FemSolver.h"

class CollisionHandler {
protected:
    std::vector<FemSolver<3>*> femSolvers;
public:
    CollisionHandler();
    void addFemObject(FemSolver<3> *fem);
    virtual void adjustFemPositions() = 0;
};


#endif //FEM_SOLVER_COLLISIONHANDLER_H
