//
// Created by Wetherbe Admin on 11/10/18.
//

#ifndef FEM_SOLVER_SPHERE_H
#define FEM_SOLVER_SPHERE_H

#include "CollisionHandler.h"
class Sphere: public CollisionHandler {
public:
    Sphere();
    Matrix<double, 3,1> startPos;
    Matrix<double, 3,1> velocity;
    double radius;
    void adjustFemPositions();
};



#endif //FEM_SOLVER_SPHERE_H
