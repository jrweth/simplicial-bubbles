//
// Created by Wetherbe Admin on 11/8/18.
//

#include "GroundPlane.h"

void GroundPlane::adjustFemPositions() {
    for(int f = 0; f < femSolvers.size(); f++) {
        for(int p =0; p < femSolvers[f]->position.size(); p++) {
            if(femSolvers[f]->position[p](1,0) < 0) {
                femSolvers[f]->position[p](1,0) = 0;
                //femSolvers[f]->velocity[p](1,0) = 0;
            }
        }
    }
}