//
// Created by Wetherbe Admin on 11/10/18.
//

#include "Platform.h"

void Platform::adjustFemPositions() {
    for(int f = 0; f < femSolvers.size(); f++) {
        for(int p =0; p < femSolvers[f]->position.size(); p++) {
            double x = femSolvers[f]->position[p](0,0);
            double y = femSolvers[f]->position[p](1,0);
            if(x >= 1 && y < height) {
                femSolvers[f]->position[p](1,0) = height;
                femSolvers[f]->velocity[p](1,0) = 0;
//                //if we are nearer the riser then adjust x
//                if(height - y  >  -x) {
//                    femSolvers[f]->position[p](0,0) = 0;
//                    //femSolvers[f]->velocity[p](0,0) = 0;
//                }
//                    //otherwise adjust y
//                else {
//                    femSolvers[f]->position[p](1,0) = height;
//                    //femSolvers[f]->velocity[p](1,0) = 0;
//                }
            }
        }
    }
}