//
// Created by Wetherbe Admin on 11/10/18.
//

#include "Sphere.h"
using std::cout;

Sphere::Sphere() {
    startPos << 2.6 , 6, 0.7;
    velocity << -0.7, 0, 0;
    radius = 0.5;
}
void Sphere::adjustFemPositions() {
    //find the center of the sphere at the current time
    Matrix<double, 3, 1> pos = startPos +  femSolvers[0]->current_time * velocity;
    cout << " " << pos(0,0);
    for(int f = 0; f < femSolvers.size(); f++) {
        for(int p =0; p < femSolvers[f]->position.size(); p++) {
            Matrix<double, 3, 1> diff = femSolvers[f]->position[p] - pos;
            if(diff.squaredNorm() < radius*radius) {
                femSolvers[f]->position[p] = pos + diff * radius / diff.norm();
                femSolvers[f]->velocity[p] << 0,0,0;
                cout << "i";
            }
        }
    }
}