//
// Created by Wetherbe Admin on 11/8/18.
//

#ifndef FEM_SOLVER_GROUNDPLANE_H
#define FEM_SOLVER_GROUNDPLANE_H


#include "CollisionHandler.h"

class GroundPlane: public CollisionHandler {
public:
    void adjustFemPositions();
};


#endif //FEM_SOLVER_GROUNDPLANE_H
