//
// Created by Wetherbe Admin on 11/19/18.
//

#include "OtherFemObjects.h"



void OtherFemObjects::adjustFemPositions() {

    int obj1, obj2;
    for(obj1 = 0; obj1 < this->femSolvers.size(); obj1++) {
        for(obj2 =0; obj2 < this->femSolvers.size(); obj2++) {
            if(obj1 != obj2) {
                collide(femSolvers[obj2], femSolvers[obj1]);
                collide(femSolvers[obj1], femSolvers[obj2]);
//                collide(femSolvers[obj1], femSolvers[obj2]);
//                collide(femSolvers[obj1], femSolvers[obj2]);
//                collide(femSolvers[obj2], femSolvers[obj1]);
//                collide(femSolvers[obj1], femSolvers[obj2]);
//                collide(femSolvers[obj2], femSolvers[obj1]);
//                collide(femSolvers[obj1], femSolvers[obj2]);
            }
        }
    }
}


void OtherFemObjects::collide(FemSolver<3> *objA, FemSolver<3> *objB) {

    //first check boudning boxes for the whole object
    BoundingBox boxA = getBoundingBox(objA);
    BoundingBox boxB = getBoundingBox(objB);
    if(!boundingBoxesIntersect(boxA, boxB)) {
        return;
    }

    //could be colliding - we need to check if the boundary points of first object penetrate the boundary tets of the second
    for(int pointA = 0; pointA < objA->boundary_points.size(); pointA++) {
        for(auto it = objB->boundary_mesh_parts.begin(); it != objB->boundary_mesh_parts.end(); ++it) {

            //first just check the bounding box
            int meshIndex = it->first;
            BoundingBox bb = getMeshBoundingBox(meshIndex, objB);

            if(positionInBoundingBox(objA->position[objA->boundary_points[pointA]], bb)) {
                if(positionInMeshPart(objA->position[objA->boundary_points[pointA]], meshIndex, objB)) {
                   // cout << endl << "m############################################";
                    adjustPosition(objA->boundary_points[pointA], objA, meshIndex, objB);
                }
            }
        }
    }
    return;

}



/**
 * Determine if two boudning boxes collide
 * @param boxA list of min xyz and max xzy for boxA
 * @param boxB list of min xyz and max xyz for box B
 * @return
 */
bool OtherFemObjects::boundingBoxesIntersect(
        BoundingBox boxA,
        BoundingBox boxB
) {

    return (boxA.minX <= boxB.maxX && boxA.maxX >= boxB.minX) &&
           (boxA.minY <= boxB.maxY && boxA.maxY >= boxB.minY) &&
           (boxA.minZ <= boxB.maxZ && boxA.maxZ >= boxB.minZ);
}

BoundingBox OtherFemObjects::getBoundingBox(FemSolver<3> *object){
    BoundingBox b;

    for(int i=0; i < object->position.size(); i++) {
        //loop over each axis and adjust the max and mins for x, y and z

        if(object->position[i][0] < b.minX) b.minX = object->position[i][0];
        if(object->position[i][1] < b.minY) b.minY = object->position[i][1];
        if(object->position[i][2] < b.minZ) b.minZ = object->position[i][2];

        if(object->position[i][0] > b.maxX) b.maxX = object->position[i][0];
        if(object->position[i][1] > b.maxY) b.maxY = object->position[i][1];
        if(object->position[i][2] > b.maxZ) b.maxZ = object->position[i][2];
    }
    return b;
}

BoundingBox OtherFemObjects::getMeshBoundingBox(int meshIndex, FemSolver<3>* solver) {
    BoundingBox b;

    for(int i=0; i < 4; i++) {
        //loop over each axis and adjust the max and mins for x, y and z
        Matrix<double, 3, 1> point = solver->position[solver->mesh[meshIndex][i]];

        if(point(0,0) < b.minX) b.minX = point(0,0);
        if(point(1,0) < b.minY) b.minY = point(1,0);
        if(point(2,0) < b.minZ) b.minZ = point(2,0);

        if(point(0,0) > b.maxX) b.maxX = point(0,0);
        if(point(1,0) > b.maxY) b.maxY = point(1,0);
        if(point(2,0) > b.maxZ) b.maxZ = point(2,0);
    }
    return b;
}

bool OtherFemObjects::positionInBoundingBox(Matrix<double, 3, 1> position, BoundingBox box) {

    //check if our point is within our bounding box
    return
            position(0,0) > box.minX && position(0,0) < box.maxX &&
            position(1,0) > box.minY && position(1,0) < box.maxY &&
            position(2,0) > box.minZ && position(2,0) < box.maxZ;

}

bool OtherFemObjects::positionInMeshPart(Matrix<double, 3, 1> position, int meshIndex, FemSolver<3> *obj) {
    //check each of the planes defined by the teterahedron
    array<array<int, 3>, 4> planePoints = {{{0,1,2},{2,1,3},{2,3,0},{0,3,1}}};
    for(int plane = 0; plane < 4; plane++) {
        Matrix<double, 3, 1> p1 = obj->position[obj->mesh[meshIndex][planePoints[plane][0]]];
        Matrix<double, 3, 1> p2 = obj->position[obj->mesh[meshIndex][planePoints[plane][1]]];
        Matrix<double, 3, 1> p3 = obj->position[obj->mesh[meshIndex][planePoints[plane][2]]];
        Matrix<double, 3, 1> e1 = p2 - p1;
        Matrix<double, 3, 1> e2 = p3 - p2;

        //check to see if the normal of the plane with the dot product of the position is positive
        //this means that the position is on the wrong side of the plane so is not in the tetrehedron
        Matrix<double, 3, 1> cross = e2.cross(e1);
        if(e2.cross(e1).dot(position-p2) >= 0) {
            return false;
        }
    }
    return true;

}

void OtherFemObjects::adjustPosition(
        int positionIndex,
        FemSolver<3> *positionObject,
        int meshIndex,
        FemSolver<3> *tetObject)
{
    BoundaryMeshPart bmp = tetObject->boundary_mesh_parts[meshIndex];
    //there is only one point on the boundary so just move the position to that point
    if(bmp.boundary_triangles.size() > 0) {
        //find closest point on the faces
        double distance;
        double closest_distance = 9999999;
        Matrix<double, 3, 1> new_position;
        Matrix<double, 3, 1> closest_position;
        for(const auto& bTriangle: bmp.boundary_triangles) {
            Matrix<double, 3, 1> *p = &positionObject->position[positionIndex];

            //find the three points of the boundary triangle
            Matrix<double, 3, 1> *p1 = &tetObject->position[tetObject->boundary_points[tetObject->boundary_triangles[bTriangle][0]]];
            Matrix<double, 3, 1> *p2 = &tetObject->position[tetObject->boundary_points[tetObject->boundary_triangles[bTriangle][1]]];
            Matrix<double, 3, 1> *p3 = &tetObject->position[tetObject->boundary_points[tetObject->boundary_triangles[bTriangle][2]]];

            closestPointOnPlane(
                    &positionObject->position[positionIndex],
                    p1,
                    p2,
                    p3,
                    &distance,
                    &closest_position
            );

            if(distance < closest_distance) {
                closest_distance = distance;
                new_position = closest_position;
            }
        }
        positionObject->position[positionIndex] = new_position;

    }
    else if(bmp.boundary_points.size() == 1) {
        cout << " 1 point ";
        positionObject->position[positionIndex] = tetObject->position[*bmp.boundary_points.begin()];
    }
    //there are only two points on the boundary so just take the closest projection
    else if (bmp.boundary_points.size() == 2) {
        cout << " 2 points ";
        Matrix<double, 3, 1> p1, p2;
        int pt = 1;
        for (auto itr = bmp.boundary_points.begin(); itr != bmp.boundary_points.end(); ++itr) {
            if(itr == bmp.boundary_points.begin()) {
                p1 = tetObject->position[*itr];
            }
            else {
                p2 = tetObject->position[*itr];
            }
        }
        Matrix<double, 3, 1> e = p2 - p1;

        double dot = (positionObject->position[positionIndex] - p1).transpose().dot(e.transpose());
//        cout << endl << "p1: " << p1.transpose();
//        cout << endl << "p2: " << p2.transpose();
//        cout << endl << "p: " << positionObject->position[positionIndex].transpose();
//        cout << endl << "e: " << e.transpose();
//        cout << endl << "e2" << (positionObject->position[positionIndex] - p1).transpose();
//        cout << endl << "e norm: " << e.norm();
//        cout << endl << "e / e.norm" << (e / e.norm()).transpose();
//        cout << endl << "dot: " << dot;
//        cout << endl << "dot e/ e.normm: " << (dot * e / e.norm()).transpose();
        //if dot product is negative then just move to point 1
        if(dot <= 0) {
            cout << "p1";
            positionObject->position[positionIndex] = p1;
        }
        //if the projection is longer than edget just use point 2
        else if(dot*dot >= e.squaredNorm()) {
            cout << "p2";
            positionObject->position[positionIndex] = p2;
        }
        //use the projection
        else {
            cout << "projecting";
            positionObject->position[positionIndex] = p1 + dot * e / e.squaredNorm();
            cout << "new point: " << positionObject->position[positionIndex].transpose();
        }
    }
    else {
        cout << "##########################################################3more than 2 points";
    }
    positionObject->velocity[positionIndex] << 0,0,0;
}

//
void OtherFemObjects::closestPointOnPlane(
        Matrix<double, 3, 1> *p, //point
        Matrix<double, 3, 1> *p1, //plane point 1
        Matrix<double, 3, 1> *p2, //plane point 2
        Matrix<double, 3, 1> *p3, //plane point 3
        double *distance,  //distance from plane
        Matrix<double, 3, 1> *positionOnPlane //new position on plane
) {
    Matrix<double, 3, 1> normal = (*p3-*p1).cross(*p2-*p1);
    //cout << endl << endl << "normal of matching face" <<  normal.transpose();
    double normalLength = normal.norm();
    double cosA = (*p - *p1).dot(normal)/((*p-*p1).norm()*normalLength);
//    cout << endl << "p: " << (*p).transpose();
//    cout << endl << "p1: " << (*p1).transpose();
//    cout << endl << "p-p1" << (*p-*p1).transpose();
//    cout << endl << "p-p1.dot normal" << (*p-*p1).dot(normal);
//    cout << endl << "cosa: " << cosa;
    *distance = -(*p1-*p).norm() * cosA;
//    cout << endl << "distance: " << *distance;
//    cout << endl << "normalalized normal" << (normal/normalLength).transpose();
    *positionOnPlane = *p + (*distance * normal / normalLength);
//    cout << endl << "position on plane" << positionOnPlane->transpose();


}
