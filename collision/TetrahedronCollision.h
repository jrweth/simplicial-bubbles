//
// Created by Wetherbe Admin on 11/19/18.
//

#ifndef FEM_SOLVER_TETRAHEDRONCOLLISION_H
#define FEM_SOLVER_TETRAHEDRONCOLLISION_H

#include "../eigen/Eigen/Dense"
#include <array>

using namespace Eigen;
using std::array;

class TetrahedronCollision {
public:
    bool tetsCollide(array<Matrix<double, 3,1>, 4> tetA, array<Matrix<double, 3,1>, 4> tetB);
};


#endif //FEM_SOLVER_TETRAHEDRONCOLLISION_H
