//
// Created by Wetherbe Admin on 11/10/18.
//

#ifndef FEM_SOLVER_PLATFORM_H
#define FEM_SOLVER_PLATFORM_H

#include "CollisionHandler.h"
class Platform: public CollisionHandler {
public:
    double height = 5.0;
    void adjustFemPositions();
};


#endif //FEM_SOLVER_PLATFORM_H
