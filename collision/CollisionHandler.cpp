//
// Created by Wetherbe Admin on 11/6/18.
//

#include "CollisionHandler.h"

CollisionHandler::CollisionHandler() {
    this->femSolvers.clear();
}
void CollisionHandler::addFemObject(FemSolver<3> *fem) {
    this->femSolvers.push_back(fem);
}