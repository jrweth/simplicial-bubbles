//
// Created by Wetherbe Admin on 11/8/18.
//

#include "Staircase.h"

void Staircase::adjustFemPositions() {
    for(int f = 0; f < femSolvers.size(); f++) {
        for(int p =0; p < femSolvers[f]->position.size(); p++) {
            double x= femSolvers[f]->position[p](0,0);
            double y = femSolvers[f]->position[p](1,0);

            //if y < x + height
            bool debug = false;
            if(y < floor(x) + height && x < 1) {

                //stuck in the corner  both x and y need adjusting back to the corner
                if(y < floor(x) + height - 1) {
                    femSolvers[f]->position[p](0,0) = floor(x);
                    femSolvers[f]->position[p](1,0) = floor(x) + height -1;
                    femSolvers[f]->velocity[p](0,0) = 0;
                    femSolvers[f]->velocity[p](1,0) = 0;
                }

                //if we are nearer the riser then adjust x
                else if(ceil(y) - y > x -floor(x)) {
                    femSolvers[f]->position[p](0,0) = floor(x);
                    femSolvers[f]->velocity[p](0,0) = 0;
                }
                //otherwise adjust y
                else {
                    femSolvers[f]->position[p](1,0) = floor(x) + height;
                    femSolvers[f]->velocity[p](1,0) = 0;
                }
                if(debug) cout << endl << "x: " << x << ", " << y << " to: " << femSolvers[f]->position[p](0,0) << ", " << femSolvers[f]->position[p](1,0);

            }
        }
    }
}