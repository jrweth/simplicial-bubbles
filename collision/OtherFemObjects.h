//
// Created by Wetherbe Admin on 11/19/18.
//

#ifndef FEM_SOLVER_OTHERFEMOBJECTS_H
#define FEM_SOLVER_OTHERFEMOBJECTS_H

#include "CollisionHandler.h"

struct BoundingBox {
    double minX = 999999999;
    double minY = 999999999;
    double minZ = 999999999;

    double maxX = -99999999;
    double maxY = -99999999;
    double maxZ = -99999999;
};

class OtherFemObjects: public CollisionHandler {
public:
    void adjustFemPositions();
    void collide(FemSolver<3> *objA, FemSolver<3> *objB);
    bool boundingBoxesIntersect(BoundingBox boxA, BoundingBox boxB);
    BoundingBox getBoundingBox(FemSolver<3> *obj);
    BoundingBox getMeshBoundingBox(int meshIndex, FemSolver<3> *obj);
    bool positionInBoundingBox(Matrix<double, 3, 1> position, BoundingBox box);
    bool positionInMeshPart(Matrix<double, 3, 1> position, int meshIndex, FemSolver<3> *obj);
    void adjustPosition(int positionIndex, FemSolver<3> *positionObject, int meshIndex, FemSolver<3> *tetObject);

    void closestPointOnPlane(
            Matrix<double, 3, 1> *p,
            Matrix<double, 3, 1> *p1,
            Matrix<double, 3, 1> *p2,
            Matrix<double, 3, 1> *p3,
            double *distance,
            Matrix<double, 3, 1> *positionOnPlane);
};


#endif //FEM_SOLVER_OTHERFEMOBJECTS_H
