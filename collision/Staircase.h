//
// Created by Wetherbe Admin on 11/8/18.
//

#ifndef FEM_SOLVER_STAIRCASE_H
#define FEM_SOLVER_STAIRCASE_H


#include "CollisionHandler.h"

class Staircase: public CollisionHandler {
public:
    double step_size = 1;
    double height = 5;
    void adjustFemPositions();
};


#endif //FEM_SOLVER_STAIRCASE_H
